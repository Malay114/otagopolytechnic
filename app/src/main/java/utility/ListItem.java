package utility;

import java.util.Date;

/**
 * Created by malay on 3/2/2017.
 */

public final class ListItem {


    private String pageId, pageTitle, pageName, Id, Title, Containt, imageData, CourseTitle,LevelNo,Duration;
    public String Intake, Apply, Credit, Delivery, Fee_Domestic, Fee_International, Fee_Note, Course_Discription;
    public String Entery_Requirement, Additional_Doc, Selection_Procedure,You_Study,You_Study2,You_Study3;
    public String Workload,Further_Study_Option,Student_Allowance,Disclaimer;
    public String Fname,Lname, Mname, prefix, Name, Designation, email, officeNo, lineExtension;
    public String mobileNo, agentId, VideoUrl, name,passportNo, descOrAddress="";
    private String id, time, type, Question,Offers;
    private String PrefferedName, Gender, EmergencyContactName, EmergencyContactRelation, EmergencyPhNo, EmergencyContactEmail, Country, IntendedCareer, EnglishCourse, Program, Gov1stStusyYear, OccupationLastYear, DisabilitySupport, Marcketing, TermsCondition, Active;
    private Date dob;
    private float rating;
    private int count;
    private double km, latitude, longitude;


     ListItem(String pageId, String pageTitle,String pageName,String Id,String Title,String Containt,String name, String descOrAddress, float rating,
                    String imageData,String id,String courseTitle,String levelNo,String duration,String intake, String apply,String credit,
              String delivery,String fee_Domestic,String fee_International,String fee_Note,String course_Discription,String entery_Requirement,
              String additional_Doc,String selection_Procedure,String you_Study,String you_Study2,String you_Study3,String workload,
              String further_Study_Option,String student_Allowance,String disclaimer,String VideoUrl,String passportNo,String Fname,String Lname,
              String Mname,Date dob,String PrefferedName,String Gender,String EmergencyContactName,String EmergencyContactRelation,String EmergencyPhNo,String EmergencyContactEmailString,String Country,String IntendedCareer,String EnglishCourse,String Program,String Gov1stStusyYear,String OccupationLastYear,String DisabilitySupport,String Marcketing,String TermsCondition,String Active)
     {
        this.pageId =pageId;
        this.pageTitle = pageTitle;
        this.passportNo=passportNo;
        this.dob=dob;
        this.Fname=Fname;
        this.Mname=Mname;
        this.Lname=Lname;
        this.agentId=agentId;
        this.pageName = pageName;
        this.Id = Id;
        this.Title = Title;
        this.Containt = Containt;
        this.imageData = imageData;
        this. CourseTitle=courseTitle;
        this.LevelNo=levelNo;
        this.Duration=duration;
        this.Intake=intake;
        this.Apply=apply;
        this. Credit=credit;
        this. Delivery=delivery;
        this. Fee_Domestic=fee_Domestic;
        this. Fee_International=fee_International;
        this. Fee_Note=fee_Note;
        this. Course_Discription=course_Discription;
        this. Entery_Requirement=entery_Requirement;
        this. Additional_Doc=additional_Doc;
        this. Selection_Procedure=selection_Procedure;
        this. You_Study=you_Study;
        this. You_Study2=you_Study2;
        this. You_Study3=you_Study3;
        this. Workload=workload;
        this. Further_Study_Option=further_Study_Option;
        this. Student_Allowance=student_Allowance;
        this. Disclaimer=disclaimer;
        this.VideoUrl=VideoUrl;
        this.prefix=prefix;
        this.Name=Name;
        this.Designation=Designation;
        this.email=email;
        this. officeNo=officeNo;
        this.lineExtension=lineExtension;
        this.mobileNo=mobileNo;
        this.name = name;
        this.descOrAddress = descOrAddress;
        this.rating = rating;
        this.id=id;
        this.km=km;
        this.latitude=latitude;
        this.longitude=latitude;
        this.time=time;
        this.type=type;
        this.PrefferedName=PrefferedName;
        this.Gender=Gender;
        this.EmergencyContactName=EmergencyContactName;
        this.EmergencyContactRelation=EmergencyContactRelation;
        this.EmergencyPhNo=EmergencyPhNo;
        this.EmergencyContactEmail=EmergencyContactEmail;
        this.Country=Country;
        this.IntendedCareer=IntendedCareer;
        this.EnglishCourse=EnglishCourse;
        this.Program=Program;
        this.Gov1stStusyYear=Gov1stStusyYear;
        this.OccupationLastYear=OccupationLastYear;
        this.DisabilitySupport=DisabilitySupport;
        this.Marcketing=Marcketing;
        this.TermsCondition=TermsCondition;
        this.Active=Active;
    }
    public ListItem() {
        pageId = "";
        pageTitle = "";
        passportNo="";
        pageName = "";
        Id = "";
        Title = "";
        Containt = "";
        imageData = "";
        CourseTitle="";
        LevelNo="";
        Duration="";
        Intake="";
        Apply="";
        Fname="";
        Mname="";
        Lname="";
        agentId="";
        Credit="";
        Delivery="";
        Fee_Domestic="";
        Fee_International="";
        Fee_Note="";
        Course_Discription="";
        Entery_Requirement="";
        Additional_Doc="";
        Selection_Procedure="";
        You_Study="";
        You_Study2="";
        You_Study3="";
        Workload="";
        Further_Study_Option="";
        Student_Allowance="";
        Disclaimer="";
        prefix="";
        Name="";
        Designation="";
        email="";
        officeNo="";
        lineExtension="";
        mobileNo="";
        VideoUrl="";
        name = "";
        descOrAddress = "";
        rating = 0;
        id="";
        km=0;
        latitude=0;
        longitude=0;
        time="";
        type="";
    }


    public String getPageId() {return pageId;}
    public void setPageId(String pageid) {this.pageId = pageid;}
    public String getPageTitle() {return pageTitle;}
    public void setPageTitle(String pageTitle) {this.pageTitle = pageTitle;}
    public String getPageName() {return pageName;}
    public void setPageName(String pageName) {this.pageName = pageName;}
    public String getId() {
        return Id;
    }
    public void setId(String Id) {
        this.Id = Id;
    }
    public String getTitle() {
        return Title;
    }
    public void setTitle(String Title1) {
        this.Title = Title1;
    }
    public String getContain() {
        return Containt;
    }
    public void setContain(String Containt) {
        this.Containt = Containt;
    }
    public String getImageData() {
        return imageData;
    }
    public void setImageData(String imageData) {
        this.imageData = imageData;
    }

    public String getCourseTitle() {
        return CourseTitle;
    }
    public void setCourseTitle(String CourseTitle) {
        this.CourseTitle = CourseTitle;
    }
    public String getLevelNo() {
        return LevelNo;
    }
    public void setLevelNo(String LevelNo) {
        this.LevelNo = LevelNo;
    }
    public String getDuration() {
        return Duration;
    }
    public void setDuration(String Duration) {
        this.Duration = Duration;
    }
    public String getIntake() {
        return Intake;
    }
    public void setIntake(String Intake) {
        this.Intake = Intake;
    }
    public String getApply() {
        return Apply;
    }
    public void setApply(String Apply) {
        this.Apply = Apply;
    }

    public String getCredit() {
        return Credit;
    }
    public void setCredit(String credit) {
        this.Credit = credit;
    }
    public String getDelivery() {
        return Delivery;
    }
    public void setDelivery(String delivery) {
        this.Delivery = delivery;
    }
    public String getFee_Domestic() {
        return Fee_Domestic;
    }
    public void setFee_Domestic(String fee_Domestic) {
        this.Fee_Domestic = fee_Domestic;
    }
    public String getFee_International() {
        return Fee_International;
    }
    public void setFee_International(String fee_International) {this.Fee_International = fee_International;}
    public String getFee_Note() {
        return Fee_Note;
    }
    public void setFee_Note(String fee_Note) {
        this.Fee_Note = fee_Note;
    }
    public String getCourse_Discription() {
        return Course_Discription;
    }
    public void setCourse_Discription(String course_Discription) {this.Course_Discription = course_Discription;}
    public String getEntery_Requirement() {
        return Entery_Requirement;
    }
    public void setEntery_Requirement(String entery_Requirement) {this.Entery_Requirement = entery_Requirement;}
    public String getAdditional_Doc() {
        return Additional_Doc;
    }
    public void setAdditional_Doc(String additional_Doc) {
        this.Additional_Doc = additional_Doc;
    }
    public String getSelection_Procedure() {
        return Selection_Procedure;
    }
    public void setSelection_Procedure(String selection_Procedure) {this.Selection_Procedure = selection_Procedure;}
    public String getYou_Study() {
        return You_Study;
    }
    public void setYou_Study(String you_Study) {
        this.You_Study = you_Study;
    }
    public String getYou_Study2() {
        return You_Study2;
    }
    public void setYou_Study2(String you_Study2) {
        this.You_Study2 = you_Study2;
    }
    public String getYou_Study3() {
        return You_Study3;
    }
    public void setYou_Study3(String you_Study3) {
        this.You_Study3 = you_Study3;
    }
    public String getWorkload() {
        return Workload;
    }
    public void setWorkload(String workload) {
        this.Workload = workload;
    }
    public String getFurther_Study_Option() {
        return Further_Study_Option;
    }
    public void setFurther_Study_Option(String further_Study_Option) { this.Further_Study_Option = further_Study_Option; }
    public String getStudent_Allowance() {
        return Student_Allowance;
    }
    public void setStudent_Allowance(String student_Allowance) {this.Student_Allowance = student_Allowance;}
    public String getDisclaimer() {return Disclaimer;}
    public void setDisclaimer(String disclaimer) {
        this.Disclaimer = disclaimer;
    }


    public String getPrefix() {return prefix;  }
    public void setPrefix(String prefix) {  this.prefix = prefix;  }
    public String getDesignation() {  return Designation;  }
    public void setDesignation(String designation) {   Designation = designation;  }
    public String getEmail() { return email; }
    public void setEmail(String email) { this.email = email; }
    public String getOfficeNo() { return officeNo; }
    public void setOfficeNo(String officeNo) { this.officeNo = officeNo;  }
    public String getLineExtension() { return lineExtension;  }
    public void setLineExtension(String lineExtension) { this.lineExtension = lineExtension; }
    public String getMobileNo() { return mobileNo;  }
    public void setMobileNo(String mobileNo) {   this.mobileNo = mobileNo;  }
    public String getName() {
        return name;
    }
    public void setName(String name) { this.name = name;  }

    public void setPassportNo(String passportNo) {this.passportNo = passportNo;}
    public String getPassportNo() {return passportNo;}
    public void setFname(String Fname) {this.Fname = Fname;}
    public String getLname() {return Lname;}
    public void setLname(String Lname) {this.Lname = Lname;}
    public String getMname() {return Mname;}
    public void setDob(Date dob) {this.dob = dob;}
    public Date getDob() {return dob;}
    public String getAgentId() {return agentId;}
    public void setAgentId(String pageid) {this.agentId = agentId;}



    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public String getVideo_Url() {return VideoUrl;}
    public void setVideoUrl(String VideoUrl){this.VideoUrl=VideoUrl;}
    public String getDescOrAddress() {
        return descOrAddress;
    }
    public void setDescOrAddress(String descOrAddress) {
        this.descOrAddress = descOrAddress;
    }
/*    public String getId() {return id;}
    public void setId(String id) {
        this.id = id;
    }*/
    public double getKm() {
        return km;
    }
    public void setKm(double km) {
        this.km = km;
    }
    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
    public double getLongitude() {
        return longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getQuestion() {
        return Question;
    }
    public void setQuestion(String question) {
        Question = question;
    }
    public float getRating() {
        return rating;
    }
    public void setRating(float rating) {
        this.rating = rating;
    }
    public String getOffers() {
        return Offers;
    }
    public void setOffers(String offers) {
        Offers = offers;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
}
