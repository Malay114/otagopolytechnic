package utility;

/**
 * Created by malay on 3/2/2017.
 */

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.example.malay.otagopolytechnic.StudentLoginFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public final class Util {
    public static Bitmap StringToBitmap(String imageData)
    {
        byte[] decodedString=null;
        Bitmap decodedByte=null;
        decodedString = Base64.decode(imageData, Base64.DEFAULT);
        decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;

    }

//Home page Json data
    public static List<ListItem> jsonTOHomePage(JSONObject jsonObj) throws JSONException
    {

        JSONArray jsonArray = jsonObj.optJSONArray("info");
        if(jsonArray!=null)
        {
            List<ListItem> listItem = new LinkedList<ListItem>();
            int length = jsonArray.length();
            for(int i=0; i < length; i++)
            {
                ListItem singleItem = new ListItem();
                JSONObject jsonChildNode = jsonArray.getJSONObject(i);

                Object jsonPageId = jsonChildNode.getInt("Page_ID");
                Object jsonPageTitle=jsonChildNode.get("Page_Title");
                Object jsonImage = jsonChildNode.get("Page_Image");

                singleItem.setPageId(jsonPageId.toString());
                singleItem.setPageTitle(jsonPageTitle.toString());
                singleItem.setImageData(jsonImage.toString());

                listItem.add(singleItem);
            }
            return listItem;
        }
        else
        {
            return null;
        }
    }

    //Get videourls

    public static List<ListItem> jsonTOVideoURL(JSONObject jsonObj) throws JSONException
    {

        JSONArray jsonArray = jsonObj.optJSONArray("info");
        if(jsonArray!=null)
        {
            List<ListItem> listItem = new LinkedList<ListItem>();
            int length = jsonArray.length();
            for(int i=0; i < length; i++)
            {
                ListItem singleItem = new ListItem();
                JSONObject jsonChildNode = jsonArray.getJSONObject(i);

                Object jsonId = jsonChildNode.getInt("ID");
                Object jsonTitle=jsonChildNode.get("Description");
                Object jsonURL = jsonChildNode.get("URL");


                singleItem.setId(jsonId.toString());
                singleItem.setTitle(jsonTitle.toString());
                singleItem.setVideoUrl("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/"+jsonURL.toString()+"\" frameborder=\"0\" allowfullscreen></iframe>");


                listItem.add(singleItem);
            }
            return listItem;
        }
        else
        {
            return null;
        }
    }





    //Get tite and its discription
    public static List<ListItem> jsonTOTiteDiscrip(JSONObject jsonObj) throws JSONException
    {

        JSONArray jsonArray = jsonObj.optJSONArray("info");
        if(jsonArray!=null)
        {
            List<ListItem> listItem = new LinkedList<ListItem>();
            int length = jsonArray.length();
            for(int i=0; i < length; i++)
            {
                ListItem singleItem = new ListItem();
                JSONObject jsonChildNode = jsonArray.getJSONObject(i);

                Object jsonId = jsonChildNode.getInt("ID");
                Object jsonTitle=jsonChildNode.get("Title");
                Object jsonContain = jsonChildNode.get("Contain");


                singleItem.setId(jsonId.toString());
                singleItem.setTitle(jsonTitle.toString());
                singleItem.setContain(jsonContain.toString());


                listItem.add(singleItem);
            }
            return listItem;
        }
        else
        {
            return null;
        }
    }


//For Course list
    public static List<ListItem> jsonTOCourseList(JSONObject jsonObj) throws JSONException
    {

        JSONArray jsonArray = jsonObj.optJSONArray("info");
        if(jsonArray!=null)
        {
            List<ListItem> listItem = new LinkedList<ListItem>();
            int length = jsonArray.length();
            for(int i=0; i < length; i++)
            {
                ListItem singleItem = new ListItem();
                JSONObject jsonChildNode = jsonArray.getJSONObject(i);

                Object jsonId = jsonChildNode.getInt("ID");
                Object jsonTitle=jsonChildNode.get("Title");
                Object jsonDuration = jsonChildNode.get("Duration");
                Object jsonLevel=jsonChildNode.get("Level");
                Object jsonIntake = jsonChildNode.get("Intake");
                Object jsonApply = jsonChildNode.get("Apply");

                singleItem.setId(jsonId.toString());
                singleItem.setCourseTitle(jsonTitle.toString());
                singleItem.setDuration(jsonDuration.toString());
                singleItem.setLevelNo(jsonLevel.toString());
                singleItem.setIntake(jsonIntake.toString());
                singleItem.setApply(jsonApply.toString());

                listItem.add(singleItem);
            }
            return listItem;
        }
        else
        {
            return null;
        }
    }



    //For Course list
    public static List<ListItem> jsonTOSingleCourse(JSONObject jsonObj) throws JSONException
    {

        JSONArray jsonArray = jsonObj.optJSONArray("info");
        if(jsonArray!=null)
        {
            List<ListItem> listItem = new LinkedList<ListItem>();
            int length = jsonArray.length();
            for(int i=0; i < length; i++)
            {
                ListItem singleItem = new ListItem();
                JSONObject jsonChildNode = jsonArray.getJSONObject(i);

                Object jsonId = jsonChildNode.getInt("ID");
                Object jsonTitle=jsonChildNode.get("Title");
                Object jsonDuration = jsonChildNode.get("Duration");
                Object jsonLevel=jsonChildNode.get("Level");
                Object jsonIntake = jsonChildNode.get("Intake");
                Object jsonApply = jsonChildNode.get("Apply");

                Object jsonCredit = jsonChildNode.get("Credit");
                Object jsonDelivery=jsonChildNode.get("Delivery");
                Object jsonFeeDomestic = jsonChildNode.get("Fee_Domestic");
                Object jsonFeeInternational=jsonChildNode.get("Fee_International");
                Object jsonFeeNote = jsonChildNode.get("Fee_Note");
                Object jsonCourseDiscription = jsonChildNode.get("Course_Discription");
                Object jsonEnteryRequirement = jsonChildNode.get("Entery_Requirement");
                Object jsonAdditionalDoc=jsonChildNode.get("Additional_Doc");
                Object jsonSelectionProcedure = jsonChildNode.get("Selection_Procedure");
                Object jsonYouStudy=jsonChildNode.get("You_Study");
                Object jsonYouStudy2 = jsonChildNode.get("You_Study2");
                Object jsonYouStudy3 = jsonChildNode.get("You_Study3");
                Object jsonWorkload = jsonChildNode.get("Workload");
                Object jsonFurtherOption=jsonChildNode.get("Further_Study_Option");
                Object jsonStudentAllowance = jsonChildNode.get("Student_Allowance");
                Object jsonDisclaimer=jsonChildNode.get("Disclaimer");

                singleItem.setId(jsonId.toString());
                singleItem.setCourseTitle(jsonTitle.toString());
                singleItem.setDuration(jsonDuration.toString());
                singleItem.setLevelNo(jsonLevel.toString());
                singleItem.setIntake(jsonIntake.toString());
                singleItem.setApply(jsonApply.toString());

                singleItem.setCredit(jsonCredit.toString());
                singleItem.setDelivery(jsonDelivery.toString());
                singleItem.setFee_Domestic(jsonFeeDomestic.toString());
                singleItem.setFee_International(jsonFeeInternational.toString());
                singleItem.setFee_Note(jsonFeeNote.toString());
                singleItem.setCourse_Discription(jsonCourseDiscription.toString());
                singleItem.setEntery_Requirement(jsonEnteryRequirement.toString());
                singleItem.setAdditional_Doc(jsonAdditionalDoc.toString());
                singleItem.setSelection_Procedure(jsonSelectionProcedure.toString());
                singleItem.setYou_Study(jsonYouStudy.toString());
                singleItem.setYou_Study2(jsonYouStudy2.toString());
                singleItem.setYou_Study3(jsonYouStudy3.toString());
                singleItem.setWorkload(jsonWorkload.toString());
                singleItem.setFurther_Study_Option(jsonFurtherOption.toString());
                singleItem.setStudent_Allowance(jsonStudentAllowance.toString());
                singleItem.setDisclaimer(jsonDisclaimer.toString());

                listItem.add(singleItem);
            }
            return listItem;
        }
        else
        {
            return null;
        }
    }
    //faculty single detail
    public static List<ListItem> jsonTOSingleFaculty(JSONObject jsonObj) throws JSONException
    {

        JSONArray jsonArray = jsonObj.optJSONArray("info");
        if(jsonArray!=null)
        {
            List<ListItem> listItem = new LinkedList<ListItem>();
            int length = jsonArray.length();
            for(int i=0; i < length; i++)
            {
                ListItem singleItem = new ListItem();
                JSONObject jsonChildNode = jsonArray.getJSONObject(i);

                Object jsonId = jsonChildNode.getInt("ID");

                Object jsonName = jsonChildNode.get("Name");
                Object jsonDesignation=jsonChildNode.get("Designation");
                Object jsonEmail = jsonChildNode.get("Email_id");
                Object jsonOfficeNo = jsonChildNode.get("OfficeContectNo");
                Object jsonLineextension = jsonChildNode.get("LineExtention");
                Object jsonMobileNo = jsonChildNode.get("MobileNo");

                singleItem.setId(jsonId.toString());

                singleItem.setName(jsonName.toString());
                singleItem.setDesignation(jsonDesignation.toString());
                singleItem.setEmail(jsonEmail.toString());
                singleItem.setOfficeNo(jsonOfficeNo.toString());
                singleItem.setLineExtension(jsonLineextension.toString());
                singleItem.setMobileNo(jsonMobileNo.toString());

                listItem.add(singleItem);
            }
            return listItem;
        }
        else
        {
            return null;
        }
    }


    //public static List<ListItem> jsonTOListItemCategory(JSONObject jsonObj) throws JSONException
    public static List<ListItem> jsonTOBlockForPage(JSONObject jsonObj) throws JSONException
    {

        JSONArray jsonArray = jsonObj.optJSONArray("info");
        if(jsonArray!=null)
        {
            List<ListItem> listItem = new LinkedList<ListItem>();
            int length = jsonArray.length();
            for(int i=0; i < length; i++)
            {
                ListItem singleItem = new ListItem();
                JSONObject jsonChildNode = jsonArray.getJSONObject(i);
                Object jsonBlockId = jsonChildNode.getInt("Block_ID");
                Object jsonPageId = jsonChildNode.getInt("Page_ID");
                Object jsonBlockTitle=jsonChildNode.get("Block_Title");
                Object jsonBlockContain = jsonChildNode.get("Block_Contain");
                Object jsonImage = jsonChildNode.get("Block_Image");

               /*singleItem.setBlockId(jsonBlockId.toString());
                singleItem.setPageId(jsonPageId.toString());
                singleItem.setBlockTitle(jsonBlockTitle.toString());
                singleItem.setBlockContain(jsonBlockContain.toString());
                singleItem.setImageData(jsonImage.toString());
                listItem.add(singleItem);*/
            }
            return listItem;
        }
        else
        {
            return null;
        }
    }
    //Student first time login json
    public static String jsonTOStudLogin(JSONObject jsonObj) throws JSONException
    {

        JSONArray jsonArray = jsonObj.optJSONArray("info");
        if(jsonArray!=null)
        {
            List<ListItem> listItem = null;
            int length = jsonArray.length();
            for(int i=0; i < length; i++)
            {
                ListItem singleItem = new ListItem();
                JSONObject jsonChildNode = jsonArray.getJSONObject(i);

                Object jsonFname = jsonChildNode.getString("First_Name");
                StudentLoginFragment.fname=jsonFname.toString();
                Object jsonLname = jsonChildNode.getString("Last_Name");
                StudentLoginFragment.lname=jsonLname.toString();
                Object jsonPassport = jsonChildNode.getString("Passport_No");
                StudentLoginFragment.passportNo=jsonPassport.toString();
                Object jsonAgentId=jsonChildNode.getString("Agent_Id");
                StudentLoginFragment.agentId=jsonAgentId.toString();
                Object jsonDOB=jsonChildNode.getString("Date_Of_Birth");
                Object jsonSID=jsonChildNode.getString("ID");
                StudentLoginFragment.sId=jsonSID.toString();

                Object jsonStudEmail = jsonChildNode.getString("Student_Email_ID");
                StudentLoginFragment.EmailId=jsonStudEmail.toString();

                singleItem.setFname(jsonFname.toString());
                singleItem.setLname(jsonLname.toString());
                singleItem.setPassportNo(jsonPassport.toString());
               // SimpleDateFormat df= new SimpleDateFormat("yyyyMMdd");
                try{
                    Date dob=new SimpleDateFormat("yyyy-MM-dd").parse(jsonDOB.toString());
                    singleItem.setDob(dob);
                    StudentLoginFragment.DOB=dob;
                }catch(ParseException ex){}

                singleItem.setEmail(jsonStudEmail.toString());
                singleItem.setAgentId(jsonAgentId.toString());
                listItem = new LinkedList<ListItem>();
                listItem.add(singleItem);
            }
            if(!listItem.equals(null))
                return "Ok";
            else
                return "";
        }
        else
        {
            return "";
        }
    }

   }
