package com.example.malay.otagopolytechnic;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.GridView;


public class Home extends AppCompatActivity
        implements  NavigationView.OnNavigationItemSelectedListener ,OnFragmentInteractionListener {

    static int pageId;
    String pageTitle;
    GridView gridView;
    FragmentManager fragmentManager;
    FragmentTransaction ft;
    Fragment frag_Cat_list;
    Fragment frag_title;
    Bundle args;
    int type;
    SqliteHelper sqliteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent HomeIntent= getIntent();
        pageId=HomeIntent.getIntExtra("Page_Id",0);
        pageTitle= HomeIntent.getStringExtra("Page_Title");
        type= HomeIntent.getIntExtra("User_Type",2);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        args = new Bundle();
        fragmentManager= getSupportFragmentManager();
        FragmentTransaction ft=fragmentManager.beginTransaction();
        sqliteHelper =new SqliteHelper(this);
        if(type==2)
            frag_title=new Login();
        else{
            sqliteHelper.getStudentLoginData();
            Menu nav_Menu = navigationView.getMenu();
            nav_Menu.findItem(R.id.nav_logan).setVisible(false);
            nav_Menu.findItem(R.id.nav_logof).setVisible(true);
            frag_title=new HomeFragment();
        }

        if(frag_title != null){
            ft=fragmentManager.beginTransaction();
            ft.replace(R.id.content_main, frag_title);
            ft.addToBackStack(null);
            ft.commit();
        }
        gridView=(GridView)findViewById(R.id.gridView1);
    }


    @Override
    public void onItemSelected(String Dept,String DeptId,int x) {


        frag_Cat_list=new FacultyFullDetailFregment();
        args.putString("Dept",Dept);
        args.putString("Dept_ID",DeptId);
        args.putInt("x",x);
//        args.putString("Block_ID",gridObj.get(position).getBlockId().toString());



        frag_Cat_list.setArguments(args);
        fragmentManager.beginTransaction()
                .replace(R.id.content_main, frag_Cat_list )
                .addToBackStack("")
                .commit();
    }
    @Override
    public void onItemSelected(String ID) {
        if(ID.equals("1")){
            frag_title=new HomeFragment();
        }
        else if(ID.equals("2")){
            frag_title=new CourseFragment();
        }
        else if(ID.equals("3")){
            frag_title=new FacilityFragment();
        }
        else if(ID.equals("4")){
            frag_title=new ServicesFragment();
        }
        else if(ID.equals("5")){
            frag_title=new AccommodationFragment();
        }
       else if(ID.equals("6")){
          Intent i1;
            i1=new Intent(Home.this,VideoPlayList.class);
            startActivity(i1);
            //  frag_title=new VideoList();
        }
        else if(ID.equals("7")){
            frag_title=new DepartmentFragment();
        }
        else if(ID.equals("9")){
            frag_title=new AbtUs();
        }
        else if(ID.equals("10")){
            frag_title=new ContectUsFragment();
        }
        else if(ID.equals("11")){
            if(LoginFragment.LoginType.equals("Agent"))
                frag_title=new AgentStudentRegiFragment();
            else if(LoginFragment.LoginType.equals("Student"))
                frag_title=new StudentApplicationFragment1();

        }

        ft=fragmentManager.beginTransaction();
        ft.replace(R.id.content_main, frag_title);
        ft.addToBackStack("");
        ft.commit();
    }

    @Override
    public void onItemSelected(String course, String Course_ID) {

        frag_title=new CousreFullDetailFregment();
        args.putString("Course_ID",Course_ID);
        frag_title.setArguments(args);
        ft=fragmentManager.beginTransaction();
        ft.replace(R.id.content_main, frag_title);
        ft.addToBackStack("");
        ft.commit();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Intent newAct=new Intent();

        if(id==R.id.nav_home) {frag_title=new HomeFragment();}
        else if(id==R.id.nav_courses) { frag_title=new CourseFragment();}
        else if(id==R.id.nav_facilities) {frag_title=new FacilityFragment();}
        else if(id==R.id.nav_servies) {frag_title=new ServicesFragment();}
        else if(id==R.id.nav_acomodate) {frag_title=new AccommodationFragment();}
        else if(id==R.id.nav_staff) {frag_title=new DepartmentFragment();}
        else if(id==R.id.nav_abtus) { frag_title=new AbtUs();}
        else if(id==R.id.nav_logan) { frag_title=new Login();}
        else if(id==R.id.nav_logof){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //Setting message manually and performing action on button click
            builder.setMessage("Do you want to close this application ?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SqliteHelper sqliteHelper =new SqliteHelper(Home.this);
                            sqliteHelper.deleteRecords();
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //  Action for 'NO' Button
                            dialog.cancel();
                        }
                    });

            //Creating dialog box
            AlertDialog alert = builder.create();
            //Setting the title manually
            alert.setTitle("LogOut");
            alert.show();
        }
        //else if(id==R.id.nav_cntus) {newAct = new Intent(Home.this, Home.class);}


        ft=fragmentManager.beginTransaction();
        ft.replace(R.id.content_main, frag_title);
        ft.addToBackStack("");
        ft.commit();
        //Intent homeListActivityIntent = new Intent(Home.this, TestActivity.class);
        //Intent newAct = new Intent(getApplicationContext(), activity.class);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
