package com.example.malay.otagopolytechnic;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import java.util.List;

import utility.ListItem;


public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {


    private int lastPosition = -1;

    List<YoutubeVideo> youtubeVideoList;
    public VideoAdapter(FragmentActivity activity, List<ListItem> listobj)
    {

    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_video, viewGroup, false);
        return new VideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoViewHolder videoViewHolder, int i) {


        videoViewHolder.videoWeb.loadData(youtubeVideoList.get(i).getVideoUrl(),"text/html","utf-8");

    }

    public VideoAdapter(List<String> youtubevideoList)
    {
        this.youtubeVideoList=youtubeVideoList;
    }


    @Override
    public int getItemCount() {
        return youtubeVideoList.size() ;
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder{
        WebView videoWeb;

        public VideoViewHolder(View itemView)
        {
            super(itemView);
            videoWeb=(WebView)itemView.findViewById(R.id.webVideoView);
            videoWeb.getSettings().setJavaScriptEnabled(true);
            videoWeb.setWebChromeClient(new WebChromeClient(){


            });
        }
    }

}
