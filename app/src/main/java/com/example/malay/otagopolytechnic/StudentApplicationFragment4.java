package com.example.malay.otagopolytechnic;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;


public class StudentApplicationFragment4 extends Fragment {
    View view;
    String Marketing;
    Bundle args;
    Button next;
    int programm;
    CheckBox ch10,ch11,ch12,ch13,ch14,ch15,ch16,ch17,ch18;
    String gender,fname,lname,mname,birthDate,country,prefix,prefferdName,email,carrer,englishCourse,govStudyYear,occupationLast,disabilitySupp,marketing,termsCon;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.student_application_regi4, container, false);
        setRetainInstance(true);

        fname = getArguments().getString("Fname");
        lname = getArguments().getString("Lname");
        mname = getArguments().getString("Mname");
        birthDate = getArguments().getString("BirthDate");
        country = getArguments().getString("Country");
        prefix = getArguments().getString("Prefix");
        email = getArguments().getString("Email");
        gender = getArguments().getString("Gender");
        prefferdName = getArguments().getString("PrefferedName");
        carrer = getArguments().getString("Career");
        englishCourse = getArguments().getString("Course");
        programm = getArguments().getInt("Programm");
        govStudyYear = getArguments().getString("GovStudyYear");
        occupationLast = getArguments().getString("OccupationLast");
        disabilitySupp = getArguments().getString("DisabilitySupp");
        termsCon = getArguments().getString("TermsCon");
        marketing = getArguments().getString("Marketing");

        ch10 = (CheckBox) view.findViewById(R.id.chk10);
        ch11 = (CheckBox) view.findViewById(R.id.chk11);
        ch12 = (CheckBox) view.findViewById(R.id.chk12);
        ch13 = (CheckBox) view.findViewById(R.id.chk13);
        ch14 = (CheckBox) view.findViewById(R.id.chk14);
        ch15 = (CheckBox) view.findViewById(R.id.chk15);
        ch16 = (CheckBox) view.findViewById(R.id.chk16);
        ch17 = (CheckBox) view.findViewById(R.id.chk17);
        ch18 = (CheckBox) view.findViewById(R.id.chk18);

        Marketing = marketing;

        next = (Button) view.findViewById(R.id.btnNext);
        args = new Bundle();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StudentApplicationFragment5 secFrag = new StudentApplicationFragment5();

                if (ch10.isChecked() == true) {
                    if (Marketing.equals(""))
                        Marketing = Marketing + "10";
                    else
                        Marketing = Marketing + "10";
                }
                if (ch11.isChecked() == true) {
                    if (Marketing.equals(""))
                        Marketing = Marketing + "11";
                    else
                        Marketing = Marketing + ",11";
                }
                if (ch12.isChecked() == true) {
                    if (Marketing.equals(""))
                        Marketing = Marketing + "12";
                    else
                        Marketing = Marketing + ",12";
                }
                if (ch13.isChecked() == true) {
                    if (Marketing.equals(""))
                        Marketing = Marketing + "13";
                    else
                        Marketing = Marketing + ",13";
                }
                if (ch14.isChecked() == true) {
                    if (Marketing.equals(""))
                        Marketing = Marketing + "14";
                    else
                        Marketing = Marketing + ",14";
                }
                if (ch15.isChecked() == true) {
                    if (Marketing.equals(""))
                        Marketing = Marketing + "15";
                    else
                        Marketing = Marketing + ",15";
                }
                if (ch16.isChecked() == true) {
                    if (Marketing.equals(""))
                        Marketing = Marketing + "16";
                    else
                        Marketing = Marketing + ",16";
                }
                if (ch17.isChecked() == true) {
                    if (Marketing.equals(""))
                        Marketing = Marketing + "17";
                    else
                        Marketing = Marketing + ",17";
                }
                if (ch18.isChecked() == true) {
                    if (Marketing.equals(""))
                        Marketing = Marketing + "18";
                    else
                        Marketing = Marketing + ",18";
                }

                args.putString("Fname", fname);
                args.putString("Mname", mname);
                args.putString("Lname", lname);
                args.putString("BirthDate", birthDate);
                args.putString("Country", country);
                args.putString("Prefix", prefix);
                args.putString("PrefferedName", prefferdName);
                args.putString("Email", fname);

                args.putString("Career", carrer);
                args.putString("Course", englishCourse);
                args.putInt("Programm", programm);
                args.putString("GovStudyYear", govStudyYear);
                args.putString("OccupationLast", occupationLast);
                args.putString("DisabilitySupp", disabilitySupp);
                args.putString("Marketing", Marketing);
                args.putString("TermsCon", termsCon);
                args.putString("Gender", gender);

                secFrag.setArguments(args);
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main, secFrag);
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });
        return view;
    }
}

