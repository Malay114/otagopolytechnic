package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import utility.Path;


public class LoginFragment extends Fragment implements AdapterView.OnItemClickListener{



    View view;
    private ProgressDialog progress;
    Button Login,registration,signup;
    EditText id,Password;
    TextView  forgotPwd;
    public static String LoginType="";
    String LoginFor;
    SqliteHelper sqliteHelper;

    private OnFragmentInteractionListener mListener;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LoginFor=getArguments().getString("Login");
        DisplayMetrics dm = new DisplayMetrics();
        view = inflater.inflate(R.layout.login_fragment, container, false);
        setRetainInstance(true);

        Login=(Button)view.findViewById(R.id.btnLogin);
        signup=(Button)view.findViewById(R.id.btnSignup);
        registration=(Button)view.findViewById(R.id.btnSignup);
        id=(EditText)view.findViewById(R.id.edtEmailId);
        Password=(EditText)view.findViewById(R.id.edtPassword);
        forgotPwd=(TextView) view.findViewById(R.id.txtForgotPwd);
        id.setFocusable(true);


        if(LoginFor.equals("Agent"))
            signup.setVisibility(View.VISIBLE);

        registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AgentRegistrationFragment1 secFrag = new AgentRegistrationFragment1();
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });

        forgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotPasswordFragment secFrag = new ForgotPasswordFragment();
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(id.getText().toString().equals("")&&Password.getText().toString().equals("")){
                    Toast.makeText(getContext(),"Password and Id cant be null", Toast.LENGTH_LONG).show();
                    //id.setFocusable(true);
                }else{
                    boolean tate=isNetworkAvailable();
                    progress = new ProgressDialog(getContext());
                    progress.setCanceledOnTouchOutside(false);
                    progress.setMessage("Loading... ");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.show();

                    String serverName= Path.getPath();
                    int userType=0;
                    if(LoginFor.equals("Agent"))
                        userType=0;
                    else  if(LoginFor.equals("Student"))
                        userType=1;
                    String str_url=serverName+"Login_Conform.php?Email_Id="+id.getText().toString()+"&Password="+Password.getText().toString()+"&User_Type="+userType;

                    new Asynkclass_Agent_login().execute(str_url);}
            }
        });

        return view;
    }

    private class Asynkclass_Agent_login extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {

            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        protected void onPostExecute(String result)
        {

            JSONObject jsonResponse;
            String response="",aid="";
            try
            {
                jsonResponse = new JSONObject(result);
                JSONObject obj = jsonResponse.optJSONObject("info");

                if(obj!=null) {
                    aid = obj.getString("ID");
                    response="Ok";
                }else {
                    response = jsonResponse.getString("info");

                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            progress.hide();

            if(response.equals("Ok")) {
                sqliteHelper =new SqliteHelper(getActivity());
                HomeFragment secFrag = new HomeFragment();
                if(LoginFor.equals("Student")){
                    LoginType="Student";
                }else if(LoginFor.equals("Agent")){
                    sqliteHelper.InsertLogin(id.getText().toString(),Password.getText().toString(),aid,0);
                    LoginType="Agent";
                }
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }else if(response.equals("No Record Found"))
                Toast.makeText(getContext(),"No record found for this email", Toast.LENGTH_LONG).show();
            else if(response.equals("Wrong Password"))
                Toast.makeText(getContext(),"Wrong password", Toast.LENGTH_LONG).show();
            else {
                Toast.makeText(getContext(),"Error in login...", Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}

