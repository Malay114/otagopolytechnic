package com.example.malay.otagopolytechnic;


import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import utility.Path;


public class StudentApplicationFragment5 extends Fragment {
    View view;
    Button previous,apply;
    private ProgressDialog progress;
    int programm;
    SqliteHelper sqliteHelper;
    EditText passport,emg_contact,emg_name,emg_email,emg_relation,password,confirmPwd;
    String fname,lname,mname,birthDate,country,prefix,prefferdName,email,gender,carrer,englishCourse,govStudyYear,occupationLast,disabilitySupp,marketing,termsCon;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.student_application_regi5, container, false);
        setRetainInstance(true);

        fname=getArguments().getString("Fname");
        lname=getArguments().getString("Lname");
        mname=getArguments().getString("Mname");
        birthDate=getArguments().getString("BirthDate");
        country=getArguments().getString("Country");
        prefix=getArguments().getString("Prefix");
        email=getArguments().getString("Email");
        gender=getArguments().getString("Gender");
        prefferdName=getArguments().getString("PrefferedName");
        carrer=getArguments().getString("Career");
        englishCourse=getArguments().getString("Course");
        programm=getArguments().getInt("Programm");
        govStudyYear=getArguments().getString("GovStudyYear");
        occupationLast=getArguments().getString("OccupationLast");
        disabilitySupp=getArguments().getString("DisabilitySupp");
        marketing=getArguments().getString("Marketing");
        termsCon=getArguments().getString("TermsCon");

        emg_contact=(EditText)view.findViewById(R.id.edtContNo);
        emg_name=(EditText)view.findViewById(R.id.edtEmrName);
        emg_relation=(EditText)view.findViewById(R.id.edtRelation);
        emg_email=(EditText)view.findViewById(R.id.edtEmrEmail);
        password=(EditText)view.findViewById(R.id.edtStdPwd);
        confirmPwd=(EditText)view.findViewById(R.id.edtConPwd);
        passport=(EditText)view.findViewById(R.id.edtPassport);
        passport.setText(StudentLoginFragment.passportNo);



        apply=(Button)view.findViewById(R.id.btnApply);

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean state=isNetworkAvailable();
                if(!state){
                    String emgRelation=emg_relation.getText().toString();
                    String emgName=emg_name.getText().toString();
                    String emgContact=emg_contact.getText().toString();
                    String emgEmail=emg_email.getText().toString();
                    String pasport=passport.getText().toString();
                    sqliteHelper =new SqliteHelper(getActivity());
                    sqliteHelper.deleteStudentRecords();
                    String query="INSERT INTO student_application (ID, First_Name, Middle_Name, Last_Name, Previous_Name, Preffered_Name, Email_Id, Gender, Passport_No, DateOfBirth, EmergencyContact_Name, EmergencyContact_Relation, EmergencyContact_PhoneNumber, EmergencyContact_Email, Country, Intended_Career, English_Course, Program, Gov_1stStusyYear, Occupation_LastYear, Disability_Support, Marcketing, Terms_Condition, Active) VALUES (NULL,'"+fname+ "','"+ mname+"','"+ lname+"','"+prefix+"','"+prefferdName+"','"+email+"','"+gender+"','"+pasport+"','"+birthDate+"','"+ emgName+"','"+emgRelation+"','"+emgContact+"','"+emgEmail+"','"+country+"','"+carrer+"','"+englishCourse+"','"+programm+"','"+govStudyYear+"','"+occupationLast+"','"+disabilitySupp+"','"+marketing+"','"+termsCon+"','Active')";
                    sqliteHelper.applicationDetail(query);
                }else {
                    progress = new ProgressDialog(getContext());
                    progress.setCanceledOnTouchOutside(false);
                    progress.setMessage("Loading... ");
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.show();
                    String serverName = Path.getPath();
                    String gender = "";
                    String str_url = serverName + "student_application.php?FName=" + fname + "&MName=" + mname + "&LName=" + lname + "&Previous_Name=" + prefix + "&Prefer_Name=" + prefferdName + "&Email_Id=" + email + "&Gender=" + gender + "&DOB=" + birthDate + "&Emg_con_Name=" + emg_name.getText().toString() + "&Emg_con_Relatio=" + emg_relation.getText().toString() + "&Emg_con_No=" + emg_contact.getText().toString() + "&Emg_con_Email=" + emg_email.getText().toString() + "&Std_Contry=" + country + "&IntedeCareer=" + carrer + "&EngCourse=" + englishCourse + "&Program=" + programm + "&FStudyYear=" + govStudyYear + "&OccLastYear=" + occupationLast + "&DisableSupport=" + disabilitySupp + "&Marcketing=" + marketing + "&Terms_Condition=" + termsCon + "&pass=" + password + "&Passport_No=" + passport + "Agent_Id=" + StudentLoginFragment.agentId;
                    new Asynkclass_stud_applictaion().execute(str_url);
                }
            }
        });
        return view;
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private class Asynkclass_stud_applictaion extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {
            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;
            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        protected void onPostExecute(String result)
        {
            JSONObject jsonResponse;
            Object response = new Object();
            try
            {
                jsonResponse = new JSONObject(result);
                response = jsonResponse.optJSONArray("info");
                Object Id = jsonResponse.getString("ID");

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            progress.hide();

            if(response!=null) {
                Toast.makeText(getContext(),"Application done successfully.", Toast.LENGTH_LONG).show();
                HomeFragment secFrag = new HomeFragment();
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
                LoginFragment.LoginType="";
            }
            else {
                Toast.makeText(getContext(),"Registration fail", Toast.LENGTH_LONG).show();
            }
        }
    }
}

