package com.example.malay.otagopolytechnic;


import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import utility.ListItem;


public class AbtUsListViewAdapter extends ArrayAdapter<ListItem>
{
        private final Activity context;
        private int lastPosition = -1;
        private List<ListItem> aboutUsList;
        private  ListItem singleItem;
        public AbtUsListViewAdapter(Activity context,List<ListItem> aboutUsList)
        {
                super(context, R.layout.abt_us_single_list);
                this.context = context;
                this.aboutUsList = aboutUsList;
        }
        @Override
        public int getCount() {
                // TODO Auto-generated method stub
                return aboutUsList.size();
        }
        @Override
        public View getView(int position, View view, ViewGroup parent)
        {

                //Animation animation = AnimationUtils.loadAnimation(getContext(), ((position%2 )==0) ? R.anim.right_from_left : R.anim.left_from_right);
                LayoutInflater inflater = context.getLayoutInflater();
                View rowView= inflater.inflate(R.layout.abt_us_single_list, null);

                singleItem= aboutUsList.get(position);

                final TextView txtTitle = (TextView) rowView.findViewById(R.id.txtTitle);
                final TextView txtDescription = (TextView) rowView.findViewById(R.id.txtDescription);
                final TextView txtId=(TextView)rowView.findViewById(R.id.txtTId);
               final String val=txtTitle.toString();

                txtTitle.setText("+  "+singleItem.getTitle());
                txtDescription.setText(singleItem.getContain());
                txtId.setText(singleItem.getId());
                if(txtTitle.getText().equals("+  ")) {
                        txtTitle.setText(" ");
                        txtTitle.setVisibility(View.GONE);
                        txtDescription.setVisibility(View.VISIBLE);
                }
                txtTitle.setOnClickListener(new View.OnClickListener() {
                        int flag = 0;

                        @Override
                        public void onClick(View v) {
                                if (flag == 0) {

                                       //txtTitle.setText("- "+val);
                                    String tp=txtTitle.getText().toString();
                                    txtTitle.setText("-  "+tp.substring(3));
                                        Log.e("****",txtTitle.getText().toString());
                                        txtDescription.setVisibility(View.VISIBLE);
                                        flag = 1;
                                } else {
                                    String tp=txtTitle.getText().toString();
                                    txtTitle.setText("+  "+tp.substring(3));
                                        txtDescription.setVisibility(View.GONE);
                                        flag = 0;
                                }
                        }
                });
                //rowView.startAnimation(animation);
                lastPosition = position;
                return rowView;
        }
}