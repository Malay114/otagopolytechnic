package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import utility.Util;

//import com.google.android.gms.location.places.AutocompletePrediction.Substring;

public class TestActivity extends Activity  implements  NavigationView.OnNavigationItemSelectedListener{
    String str1="",str2="",str3="",str4="",str5="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);

        Intent HomeIntent= getIntent();



        str2= HomeIntent.getStringExtra("Page_ID");
        Log.e("str2",""+str2);
        str3= HomeIntent.getStringExtra("Page_Title");
        Log.e("str3",""+str3);
        str5= HomeIntent.getStringExtra("Page_Image");
        Log.e("str5",""+str5);


        str1=HomeIntent.getStringExtra("Block_ID");
        Log.e("str1",""+str1);
        str4= HomeIntent.getStringExtra("Block_Contain");
        Log.e("str4",""+str4);
        //Toast.makeText(getApplicationContext(),"sucess", Toast.LENGTH_LONG).show();

        TextView txt1=(TextView) findViewById(R.id.textView2);
        TextView txt2=(TextView) findViewById(R.id.textView3);
        TextView txt3=(TextView) findViewById(R.id.textView4);
        TextView txt4=(TextView) findViewById(R.id.textView5);
        ImageView img=(ImageView) findViewById(R.id.imageView2);

        txt1.setText(str1);
        txt2.setText(str2);
        txt3.setText(str3);
        txt4.setText(str4);
        img.setImageBitmap(Util.StringToBitmap(str5));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        Intent newAct=new Intent();

        if(id==R.id.nav_home) {newAct = new Intent(this, Home.class);}/*
        else if(id==R.id.nav_courses) {newAct = new Intent(Home.this, Home.class);}
        else if(id==R.id.nav_facilities) {newAct = new Intent(Home.this, Home.class);}
        else if(id==R.id.nav_servies) {newAct = new Intent(Home.this, Home.class);}
        else if(id==R.id.nav_acomodate) {newAct = new Intent(Home.this, Home.class);}
        else if(id==R.id.nav_staff) {newAct = new Intent(Home.this, Home.class);}
        else if(id==R.id.nav_abtus) {newAct = new Intent(Home.this, Home.class);}
        else if(id==R.id.nav_cntus) {newAct = new Intent(Home.this, Home.class);}*/

        //Intent homeListActivityIntent = new Intent(Home.this, TestActivity.class);
        //Intent newAct = new Intent(getApplicationContext(), activity.class);
        startActivity(newAct);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
