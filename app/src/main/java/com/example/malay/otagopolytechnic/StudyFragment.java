package com.example.malay.otagopolytechnic;


import android.app.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import utility.ListItem;
import utility.Path;
import utility.Util;

public class StudyFragment extends Fragment implements AdapterView.OnItemSelectedListener{

    View view;
    String str1,str2,str3,str4,str5;
    private ProgressDialog progress;
    static int pageId;
    String pageTitle;

    GridView gridView;
    int[] images={/*R.drawable.banner1, R.drawable.banner2,R.drawable.banner3 ,R.drawable.banner4,
            R.drawable.banner5,R.drawable.banner6,R.drawable.banner7,R.drawable.banner8*/};


    private List<ListItem> listobj = new ArrayList<>();
    private List<String> bannerObj = new ArrayList<>();
    private List<ListItem> gridObj = new ArrayList<>();
    int bannerCount=0;
    private ImageView imge_banner;
    public int currentimageindex=0;
    private OnFragmentInteractionListener mListener;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        DisplayMetrics dm = new DisplayMetrics();
        view = inflater.inflate(R.layout.study_fragment, container, false);

        String serverName= Path.getPath();

        //String str_url=serverName+"Page_contain.php?Page_ID="+pageId;
        String str_url=serverName+"Page_contain.php?Page_ID=1";

        new Asynkclass_study().execute(str_url);
        str1=getArguments().getString("Block_ID");
        Log.e("str1",""+str1);
        str2= getArguments().getString("Page_ID");
        Log.e("str2",""+str2);
        str3= getArguments().getString("Block_Title");
        Log.e("str3",""+str3);
        str4= getArguments().getString("Block_Contain");
        Log.e("str4",""+str4);
        str5= getArguments().getString("Block_Image");
        Log.e("str5",""+str5);
        //Toast.makeText(getApplicationContext(),"sucess", Toast.LENGTH_LONG).show();

        TextView txt1=(TextView) view.findViewById(R.id.textView2);
        TextView txt2=(TextView) view.findViewById(R.id.textView3);
        TextView txt3=(TextView) view.findViewById(R.id.textView4);
        TextView txt4=(TextView) view.findViewById(R.id.textView5);
        ImageView img=(ImageView)view. findViewById(R.id.imageView2);

        txt1.setText(str1);
        txt2.setText(str2);
        txt3.setText(str3);
        txt4.setText(str4);
        img.setImageBitmap(Util.StringToBitmap(str5));

        return view;
    }

    private class Asynkclass_study extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {

            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                    Log.e("tag8",""+sb.toString());
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        protected void onPostExecute(String result)
        {

            JSONObject jsonResponse;
            try
            {
                jsonResponse = new JSONObject(result);

                listobj= Util.jsonTOBlockForPage(jsonResponse);
                int i=0;
                for(ListItem list:listobj){
                    /*if(list.getBlockTitle().equals("Image Tile")){
                        bannerObj.add(list.getImageData());
                    }*/

                    {
                        gridObj.add(list);
                    }
                }
                Log.e("listobj",""+gridObj);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            progress.hide();
            if(bannerObj!=null){
//                imge_banner = (ImageView)findViewById(R.id.img_baner);
//                ((Animatable) imge_banner.getDrawable()).start();

            }

            if(gridObj!=null)
            {
                GridViewAdapter adapter = new GridViewAdapter(getActivity(), gridObj);
                gridView.setAdapter(adapter);

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position,
                                            long id) {
                        // TODO Auto-generated method stub
                        //Intent categoryListActivityIntent = new Intent(CategListActivity.this, SubCategoryListActivity.class);
                        Intent homeListActivityIntent = new Intent(getContext(), TestActivity.class);

                      //  homeListActivityIntent.putExtra("Block_ID",gridObj.get(position).getBlockId().toString());
                        homeListActivityIntent.putExtra("Page_ID",gridObj.get(position).getPageId().toString());
                        //homeListActivityIntent.putExtra("Block_Title",gridObj.get(position).getBlockTitle().toString());
                        //homeListActivityIntent.putExtra("Block_Contain",gridObj.get(position).getBlockContain().toString());
                        homeListActivityIntent.putExtra("Block_Image",gridObj.get(position).getImageData().toString());

                        startActivity(homeListActivityIntent);


//                        frag_Cat_list=new StudyFragment();
//
//                        args.putString("Block_ID",gridObj.get(position).getBlockId().toString());
//                        args.putString("Page_ID",gridObj.get(position).getPageId().toString());
//                        args.putString("Block_Title",gridObj.get(position).getBlockTitle().toString());
//                        args.putString("Block_Contain",gridObj.get(position).getBlockContain().toString());
//                        args.putString("Block_Image",gridObj.get(position).getImageData().toString());
//                        frag_Cat_list.setArguments(args);
//                        fragmentManager.beginTransaction()
//                                .replace(R.id.content_main, frag_Cat_list )
//                                .addToBackStack("")
//                                .commit();
                    }
                });
            }
            else
            {
                Toast.makeText(getContext(),"No data to display", Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    /*@Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
           /* String name = listItem.get(position).getName();
            int image_Id = listItem.get(position).getImageId();
            long mrp = listItem.get(position).getmrp();
            long special_price = listItem.get(position).getSpecialPrice();
            //mListener.onItemSelected(name, );
        }
    }*/

    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {

        super.onDetach();
        mListener = null;
    }
}




