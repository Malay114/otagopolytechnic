package com.example.malay.otagopolytechnic;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Date;

import utility.ListItem;


public class SqliteHelper extends SQLiteOpenHelper
{
    public static final String TAG = "SqliteHelper.java";

    public static final String Login = "Login";
    public static final String COLUMN_ID="id";
    public static final String COLUMN_EmailId="EmailId";
    public static final String COLUMN_TYPE="UserType";
    public static final String COLUMN_PWD="Password";
    public static final String COLUMN_PID="PID";
    public static final String DATABASE_NAME="OtagoDb.db";

    private static final int DATABASE_VERSION = 2;
    public SqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL("DROP TABLE IF EXISTs student_application");
        final String DATABASE_CREATE="create table "+Login+"(" + COLUMN_ID + " Integer Primary Key," + COLUMN_EmailId
                +" text,"+ COLUMN_PWD +" text,"+ COLUMN_PID +" text, "+ COLUMN_TYPE +" Integer not null, Active text not null)";
        database.execSQL(DATABASE_CREATE);

        String studentLoginDetail="CREATE TABLE IF NOT EXISTS Student_Detail (ID Integer PRIMARY KEY AUTOINCREMENT,First_Name String,Last_Name String,Passport_No String,Date_Of_Birth date,Student_Email_ID String,Agent_Id String)";
        database.execSQL(studentLoginDetail);

        String studentApplication="CREATE TABLE IF NOT EXISTS student_application (ID Integer PRIMARY KEY,First_Name String NOT NULL,Middle_Name String NOT NULL,Last_Name String NOT NULL,Previous_Name String,Preffered_Name String, Email_Id String NOT NULL, Gender String NOT NULL,Passport_No String,DateOfBirth Date,EmergencyContact_Name String NOT NULL,EmergencyContact_Relation String NOT NULL,EmergencyContact_PhoneNumber String NOT NULL,EmergencyContact_Email String NOT NULL,Country String NOT NULL,Intended_Career String NOT NULL,English_Course String NOT NULL Default 'False',Program String NOT NULL,Gov_1stStusyYear String NOT NULL Default 'False',Occupation_LastYear String NOT NULL,Disability_Support String NOT NULL Default 'False',Marcketing String NOT NULL,Terms_Condition String NOT NULL,Application_Date Date,Active String Default 'Active')";
        database.execSQL(studentApplication);

    }
    public void droptable()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTs Login");
    }
    public void InsertLogin(String email,String password,String pid,int type)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query="insert into Login (EmailId,Password,PID,UserType,Active)values('"+email+"','"+password+"','"+pid+"','"+type+"','Active')";
        db.execSQL(query);
    }
    public void InsertStudLogin(String Fname, String Lname, String passport, Date dob,String email, String  pid,String sid)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String query="insert into Student_Detail (ID,First_Name,Last_Name,Passport_No,Date_Of_Birth,Student_Email_ID,Agent_Id)values('"+sid+"','"+Fname+"','"+Lname+"','"+passport+"','"+dob+"','"+email+"','"+pid+"')";
        db.execSQL(query);
    }

    public void applicationDetail(String query){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
    }

    public void deleteRecords(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from Login");
    }
    public void deleteStudentRecords(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from Student_Detail");
    }

    public String GetEmail()
    {
        String sql = "";
        sql += "SELECT * FROM Login";

        SQLiteDatabase db = this.getWritableDatabase();
        String email=null;
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.getCount()>0)
        {
            cursor.moveToFirst();
            email = cursor.getString(cursor.getColumnIndex("EmailId"));
        }
        else
        {
            email="";
        }
        return email;

    }
    public ListItem getStudentApplicationData(){
        ListItem student =new ListItem();
        String sql = "";
        sql += "SELECT * FROM student_application";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.getCount()>0) {
            student.setFname(cursor.getString(cursor.getColumnIndex("First_Name")));
            student.setFname(cursor.getString(cursor.getColumnIndex("Middle_Name")));
            student.setLname(cursor.getString(cursor.getColumnIndex("Last_Name")));
            student.setPassportNo(cursor.getString(cursor.getColumnIndex("Passport_No")));
            student.setEmail(cursor.getString(cursor.getColumnIndex("Email_Id")));
            String dt=cursor.getString(cursor.getColumnIndex("DateOfBirth"));
            try {
                //Date dob=new SimpleDateFormat("yyyy-MM-dd").parse(dt);
                Date dob=new Date(dt);
                student.setDob(dob);
            }catch (Exception e){
                e.printStackTrace();
            }
//            ID,  , Preffered_Name, Gender, , , EmergencyContact_Name, EmergencyContact_Relation, EmergencyContact_PhoneNumber, EmergencyContact_Email, Country, Intended_Career, English_Course, Program, Gov_1stStusyYear, Occupation_LastYear, Disability_Support, Marcketing, Terms_Condition, Active
            student.setAgentId(cursor.getString(cursor.getColumnIndex("First_Name")));
            student.setPrefix(cursor.getString(cursor.getColumnIndex("Previous_Name")));
//            student.set(cursor.getString(cursor.getColumnIndex("Passport_No")));
        }
        return student;
    }
    public void getStudentLoginData(){
        String sql = "";
        sql += "SELECT * FROM Student_Detail";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.getCount()>0)
        {
            cursor.moveToFirst();
            StudentLoginFragment.fname=cursor.getString(cursor.getColumnIndex("First_Name"));
            StudentLoginFragment.lname=cursor.getString(cursor.getColumnIndex("Last_Name"));
            StudentLoginFragment.passportNo=cursor.getString(cursor.getColumnIndex("Passport_No"));
            StudentLoginFragment.agentId=cursor.getString(cursor.getColumnIndex("Agent_Id"));
            StudentLoginFragment.sId=cursor.getString(cursor.getColumnIndex("ID"));
            StudentLoginFragment.EmailId=cursor.getString(cursor.getColumnIndex("Student_Email_ID"));
            String dt=cursor.getString(cursor.getColumnIndex("Date_Of_Birth"));
            try {
                //Date dob=new SimpleDateFormat("yyyy-MM-dd").parse(dt);
                Date dob=new Date(dt);
                StudentLoginFragment.DOB=dob;
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
    public String GetPid()
    {
        String sql = "";
        sql += "SELECT * FROM Login";

        SQLiteDatabase db = this.getWritableDatabase();
        String pid=null;
        Cursor cursor = db.rawQuery(sql, null);

        if (cursor.getCount()>0)
        {
            cursor.moveToFirst();
            pid = cursor.getString(cursor.getColumnIndex("PID"));
        }
        else
        {
            pid="";
        }
        return pid;

    }

    public String ActiveOrNot()
    {
        String sql = "";
        sql += "SELECT * FROM Login";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sql, null);
        String status=null;
        if (cursor.getCount()>0)
        {
            cursor.moveToFirst();
            status = cursor.getString(cursor.getColumnIndex("Active"));

        }
        else
        {
            status="";
        }
        return status;

    }
    public String GetPassword()
    {
        String sql = "";
        sql += "SELECT * FROM Login";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sql, null);
        String pwd=null;
        if (cursor.getCount()>0)
        {
            cursor.moveToFirst();
            pwd = cursor.getString(cursor.getColumnIndex("Password"));

        }
        else
        {
            pwd="";
        }
        return pwd;

    }
    public Integer GetUserType()
    {
        String sql = "";
        sql += "SELECT * FROM Login";

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(sql, null);
        Integer type=null;
        if (cursor.getCount()>0)
        {
            cursor.moveToFirst();
            type = cursor.getInt(cursor.getColumnIndex("UserType"));
        }
        else
        {
            type=2;
        }
        return type;

    }
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        //database.execSQL("DROP TABLE IF EXISTS " + Login);
        onCreate(database);
    }
}


