package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;


public class AgentRegistrationFragment2 extends Fragment implements AdapterView.OnItemClickListener{
    View view;
    EditText city,pincode,emil, password,contactNo,addressLine1,addressLine2,addressLine3;
    Button next;
    String Fname,Mname,Lname,AgencyName,country;
    Bundle args;

    private OnFragmentInteractionListener mListener;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fname=getArguments().getString("Fname");
        Mname=getArguments().getString("Mname");
        Lname=getArguments().getString("Lname");
        AgencyName=getArguments().getString("AgencyName");

        DisplayMetrics dm = new DisplayMetrics();
        view = inflater.inflate(R.layout.agent_registration2, container, false);
        setRetainInstance(true);

        next=(Button)view.findViewById(R.id.btnNext2);

        city=(EditText)view.findViewById(R.id.edtCity);
        pincode=(EditText)view.findViewById(R.id.edtPincode);
        emil=(EditText)view.findViewById(R.id.edtEmail);
        addressLine1=(EditText)view.findViewById(R.id.edtAddress1);
        addressLine2=(EditText)view.findViewById(R.id.edtAddress2);
        addressLine3=(EditText)view.findViewById(R.id.edtAddress3);
        password=(EditText)view.findViewById(R.id.edtDOB);
        contactNo=(EditText)view.findViewById(R.id.edtContact);
        args = new Bundle();
        Spinner spnCountry=(Spinner)view.findViewById(R.id.spnCountry);
        List<String> countryList=new ArrayList<String>();
        countryList.add("Afghanistan");
        countryList.add("Albania");
        countryList.add("Algeria");
        countryList.add("Andorra");
        countryList.add("Angola");
        countryList.add("Antigua and Barbuda");
        countryList.add("Argentina");
        countryList.add("Armenia");
        countryList.add("Australia");
        countryList.add("Austria");
        countryList.add("Bahamas");
        countryList.add("Bahrain");
        countryList.add("Bangladesh");
        countryList.add("Barbados");
        countryList.add("Belarus");
        countryList.add("China");
        countryList.add("Colombia");
        countryList.add("Iceland");
        countryList.add("India");
        countryList.add("Indonesia");
        countryList.add("Iraq");
        countryList.add("Ireland");
        countryList.add("Israel");
        countryList.add("Italy");
        countryList.add("Jamaica");
        countryList.add("Japan");
        countryList.add("Jordan");
        countryList.add("Kazakhstan");
        countryList.add("Kenya");
        countryList.add("Kiribati");
        countryList.add("Korea");

        ArrayAdapter adptrCountry=new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,countryList);
        spnCountry.setAdapter(adptrCountry);
        spnCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country= parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        adptrCountry.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AgentRegistrationFragment3 secFrag = new AgentRegistrationFragment3();
                args.putString("Fname",Fname);
                args.putString("Mname",Mname);
                args.putString("Lname",Lname);
                args.putString("AgencyName",AgencyName);
                args.putString("City",city.getText().toString());
                args.putString("Country",country);
                args.putString("Pincode",pincode.getText().toString());
                args.putString("Address1",addressLine1.getText().toString());
                args.putString("Address2",addressLine2.getText().toString());
                args.putString("Address3",addressLine3.getText().toString());
                args.putString("ContactNo",contactNo.getText().toString());
                secFrag.setArguments(args);
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });
        return view;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public void onResume() {
        super.onResume();
    }
    @Override
    public void onPause() {
        super.onPause();
    }
}

