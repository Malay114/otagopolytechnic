package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import utility.ListItem;


public class FacultyFullDetailAdapter extends ArrayAdapter<ListItem>
{
    private final Activity context;
    private int lastPosition = -1;
    private List<ListItem> blockgList;
    public FacultyFullDetailAdapter(Activity context, List<ListItem> blockgList)
    {
        super(context, R.layout.faculty_singleview);
        this.context = context;
        this.blockgList = blockgList;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return blockgList.size();
    }
    @Override
    public View getView(int position, View view, ViewGroup parent)
    {

        //Animation animation = AnimationUtils.loadAnimation(getContext(), ((position%2 )==0) ? R.anim.right_from_left : R.anim.left_from_right);

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.faculty_singleview, null);

        TextView txtfName = (TextView) rowView.findViewById(R.id.txtFacultyName);
        TextView txtDesig = (TextView) rowView.findViewById(R.id.txtDesignation);
        TextView txtEmail = (TextView) rowView.findViewById(R.id.txtEmail);
        TextView txtOfficeNo = (TextView) rowView.findViewById(R.id.textOfficeContact);
        TextView txtLineExt = (TextView) rowView.findViewById(R.id.txtLineextension);
        TextView txtMobile = (TextView) rowView.findViewById(R.id.txtMobileNo);


        ListItem singleItem= blockgList.get(position);

        txtfName.setText(singleItem.getName());
        txtDesig.setText(singleItem.getDesignation());
        txtEmail.setText(singleItem.getEmail());
        txtOfficeNo.setText(singleItem.getOfficeNo());

        String Ext ="<b> ext.</b>"+singleItem.getLineExtension();
        txtLineExt.setText(Html.fromHtml(Ext));
        if(txtLineExt.equals("<b> ext.</b>"))
        {
            txtOfficeNo.setVisibility(View.GONE);
            txtLineExt.setVisibility(View.GONE);
        }
        txtMobile.setText(singleItem.getMobileNo());
        if(txtMobile.equals(""))
        {
            txtOfficeNo.setVisibility(View.GONE);
            txtLineExt.setVisibility(View.GONE);
        }
        else
        {
            String mob ="<b> Mo. </b>"+singleItem.getMobileNo();
            txtMobile.setText(Html.fromHtml(mob));
        }
        //imageView.setImageBitmap(Util.StringToBitmap(singleItem.getContain()));

        lastPosition = position;
        return rowView;
    }
}