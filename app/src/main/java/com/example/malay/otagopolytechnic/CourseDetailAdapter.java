package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import utility.ListItem;


public class CourseDetailAdapter extends ArrayAdapter<ListItem>
{
    private final Activity context;

    private int lastPosition = -1;
    private List<ListItem> blockgList;
    public CourseDetailAdapter(Activity context, List<ListItem> blockgList)
    {
        super(context, R.layout.course_full_detail_fragment);
        this.context = context;
        this.blockgList = blockgList;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return blockgList.size();
    }
    @Override
    public View getView(int position, View view, ViewGroup parent)
    {

        //Animation animation = AnimationUtils.loadAnimation(getContext(), ((position%2 )==0) ? R.anim.right_from_left : R.anim.left_from_right);
        String empty="";
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.course_full_detail_fragment, null);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txtCorseName);
        TextView txtBasic = (TextView) rowView.findViewById(R.id.txtCorseBasic);
        TextView txtIntakeM = (TextView) rowView.findViewById(R.id.txtCourseIntakemonth);
        TextView txtIntake = (TextView) rowView.findViewById(R.id.txtCourseIntake);
        TextView txtDelivery = (TextView) rowView.findViewById(R.id.txtCourseDelivWher);
        TextView txtFee = (TextView) rowView.findViewById(R.id.txtCourseFees);
        TextView txtDis = (TextView) rowView.findViewById(R.id.txtCourseDis);
      //  TextView txtEntReq = (TextView) rowView.findViewById(R.id.txtCourseEntReq);
        //final TextView txtEntReqD = (TextView) rowView.findViewById(R.id.txtCourseEntReqD);
       // TextView txtApply = (TextView) rowView.findViewById(R.id.txtCourseApply);


        ListItem singleItem= blockgList.get(position);
        txtTitle.setText(singleItem.getCourseTitle());

        //String empty="";
        String Basic=" <b>Level :</b> "+singleItem.getLevelNo();
        Basic+="     <br>  <b>Credit :</b> " +singleItem.getCredit()+"<br>";
        Basic+=" <b>Duration :</b> "+singleItem.getDuration();
        txtBasic.setText(Html.fromHtml(Basic));
        txtIntakeM.setText(singleItem.getIntake());
        if(txtIntakeM.equals(""))
        {
            txtIntake.setVisibility(View.GONE);
            txtIntakeM.setVisibility(View.GONE);
        }


        Log.e("*********",singleItem.getIntake()+"**"+empty);
        txtDelivery.setText(singleItem.getDelivery());

        String Fee=" Domestic : "+singleItem.getFee_Domestic()+"<br>";
        Fee+=" International : "+singleItem.getFee_International()+"<br>";
        Fee+="<font size=\"1\" color=\"blue\">"+singleItem.getFee_Note()+"</font>";
        txtFee.setText(Html.fromHtml(Fee));


        txtDis.setText(singleItem.getCourse_Discription());
        /*txtEntReqD.setText(singleItem.getEntery_Requirement());
        //txtEntReq.setOnClickListener(new View.OnClickListener() {
            int flag = 0;

            @Override
            public void onClick(View v) {
                if (flag == 0) {


//                    txtEntReqD.setVisibility(View.VISIBLE);
                    flag = 1;
                } else {
                    // txtTitle.setText("+ "+singleItem.getTitle());
                    txtEntReqD.setVisibility(View.GONE);
                    flag = 0;
                }
            }
        });
      /*  String apply=txtApply.toString();
        apply+=singleItem.getApply();
        txtApply.setText(apply);
*/


        //imageView.setImageBitmap(Util.StringToBitmap(singleItem.getContain()));


        lastPosition = position;
        return rowView;
    }
}