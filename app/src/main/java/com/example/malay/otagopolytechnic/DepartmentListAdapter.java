package com.example.malay.otagopolytechnic;



import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import utility.ListItem;
import utility.Util;

public class DepartmentListAdapter extends ArrayAdapter<ListItem>
{
    private final Activity context;
    private int lastPosition = -1;
    private List<ListItem> blockgList;
    public DepartmentListAdapter(Activity context, List<ListItem> blockgList)
    {
        super(context, R.layout.department_single_list_item);
        this.context = context;
        this.blockgList = blockgList;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return blockgList.size();
    }
    @Override
    public View getView(int position, View view, ViewGroup parent)
    {

        //Animation animation = AnimationUtils.loadAnimation(getContext(), ((position%2 )==0) ? R.anim.right_from_left : R.anim.left_from_right);

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.department_single_list_item, null);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt_name_grid);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imagegrid1);
        ListItem singleItem= blockgList.get(position);
        txtTitle.setText(singleItem.getTitle());
        imageView.setImageBitmap(Util.StringToBitmap(singleItem.getContain()));
        //rowView.startAnimation(animation);
        lastPosition = position;
        return rowView;
    }
}