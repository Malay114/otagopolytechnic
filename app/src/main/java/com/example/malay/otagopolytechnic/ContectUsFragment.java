package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import utility.ListItem;
import utility.Path;
import utility.Util;


public class ContectUsFragment  extends Fragment implements AdapterView.OnItemClickListener{
    View view;
    private ProgressDialog progress;
    ListView SingleCourseDetail;
    public static String co_id;
    private List<ListItem> listobj = new ArrayList<>();
    private OnFragmentInteractionListener mListener;


    String Detail="";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String CorseId=getArguments().getString("Course_ID");
        Log.e("","(((((((((((((((((((((((((((((((((((((((((((("+CorseId);
        Detail="<font size=\"3\" color=\"blue\"><b>Auckland International Campus</b></font><br><b>International :</b> +64 9 309 0301<br>" +
                "<b>New Zealand :</b>0800 111 212<br>" +
                "<b>Email : </b> auckland@op.ac.nz <br>" +
                "<br>" +
                "<b>Physical address : </b>Level 2, 350 Queen Street,<br> Auckland,<br> New Zealand 1141<br>" +
                "<b>Postal address :</b> PO Box 5268,<br> Auckland,<br> New Zealand 1141";
        DisplayMetrics dm = new DisplayMetrics();
        view = inflater.inflate(R.layout.contact_us_fragment, container, false);
        setRetainInstance(true);
        TextView txtCntUs=(TextView)view.findViewById(R.id.txtConactUs);
        txtCntUs.setText(Html.fromHtml(Detail));

        //   SingleCourseDetail = (ListView) view.findViewById(R.id.list1);
       // AbtUsListViewAdapter adapter = new AbtUsListViewAdapter(getActivity(), listobj);

        if(listobj.isEmpty())
        {
            progress = new ProgressDialog(getContext());
            progress.setCanceledOnTouchOutside(false);
            progress.setMessage("Loading... ");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            progress.show();

            String serverName = Path.getPath();
         //   String str_url = serverName + "Course_Detail.php?Course_Id="+CorseId;

           // new ContectUsFragment().Asynkclass_home().execute(str_url);
            co_id=CorseId;
        }
        else
        {
            if(co_id==CorseId) {

           //     SingleCourseDetail.setAdapter(adapter);
                SingleCourseDetail.setOnItemClickListener(ContectUsFragment.this);
            }
            else{
                progress = new ProgressDialog(getContext());
                progress.setCanceledOnTouchOutside(false);
                progress.setMessage("Loading... ");
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setIndeterminate(true);
                progress.show();

                String serverName = Path.getPath();
                //String str_url = serverName + "Course_Detail.php?Course_Id="+CorseId;

                //new ContectUsFragment().Asynkclass_home().execute(str_url);
                co_id=CorseId;
            }
        }
        return view;
    }

    private class Asynkclass_home extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {

            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        protected void onPostExecute(String result)
        {

            JSONObject jsonResponse;
            try
            {
                jsonResponse = new JSONObject(result);
                listobj= Util.jsonTOSingleCourse(jsonResponse);
                Log.e("3333","(((((((((((((((((((((((((((((((((((((((((((("+listobj);

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            progress.hide();

            if(listobj!=null) {
                CourseDetailAdapter adapter = new CourseDetailAdapter(getActivity(), listobj);
                SingleCourseDetail.setAdapter(adapter);
                SingleCourseDetail.setOnItemClickListener(ContectUsFragment.this);
            }
            else {
                Toast.makeText(getContext(),"No data to display", Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}

