package com.example.malay.otagopolytechnic;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;


public class StudentApplicationFragment3 extends Fragment {
    View view;
    Button next;
    Bundle args;
    String Marketing;
    int programm;
    CheckBox ch1,ch2,ch3,ch4,ch5,ch6,ch7,ch8,ch9;
    String fname,lname,mname,birthDate,gender,country,prefix,prefferdName,email,carrer,englishCourse,govStudyYear,occupationLast,disabilitySupp,termsCon;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.student_application_regi3, container, false);
        setRetainInstance(true);

        fname=getArguments().getString("Fname");
        lname=getArguments().getString("Lname");
        mname=getArguments().getString("Mname");
        birthDate=getArguments().getString("BirthDate");
        country=getArguments().getString("Country");
        prefix=getArguments().getString("Prefix");
        email=getArguments().getString("Email");
        gender=getArguments().getString("Gender");
        prefferdName=getArguments().getString("PrefferedName");
        carrer=getArguments().getString("Career");
        englishCourse=getArguments().getString("Course");
        programm=getArguments().getInt("Programm");
        govStudyYear=getArguments().getString("GovStudyYear");
        occupationLast=getArguments().getString("OccupationLast");
        disabilitySupp=getArguments().getString("DisabilitySupp");
        termsCon=getArguments().getString("TermsCon");

        next=(Button)view.findViewById(R.id.btnNext);

        ch1=(CheckBox)view.findViewById(R.id.chk1);
        ch2=(CheckBox)view.findViewById(R.id.chk2);
        ch3=(CheckBox)view.findViewById(R.id.chk3);
        ch4=(CheckBox)view.findViewById(R.id.chk4);
        ch5=(CheckBox)view.findViewById(R.id.chk5);
        ch6=(CheckBox)view.findViewById(R.id.chk6);
        ch7=(CheckBox)view.findViewById(R.id.chk7);
        ch8=(CheckBox)view.findViewById(R.id.chk8);
        ch9=(CheckBox)view.findViewById(R.id.chk9);

        Marketing=new String ();
        Marketing="";
        args = new Bundle();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ch1.isChecked()==true){
                    Marketing=Marketing+"1";
                }if(ch2.isChecked()==true){
                    if(Marketing.equals(""))
                        Marketing=Marketing+"2";
                    else
                        Marketing=Marketing+",2";
                }if(ch3.isChecked()==true){
                    if(Marketing.equals(""))
                        Marketing=Marketing+"3";
                    else
                        Marketing=Marketing+",3";
                }if(ch4.isChecked()==true){
                    if(Marketing.equals(""))
                        Marketing=Marketing+"4";
                    else
                        Marketing=Marketing+",4";
                }if(ch5.isChecked()==true){
                    if(Marketing.equals(""))
                        Marketing=Marketing+"5";
                    else
                        Marketing=Marketing+",5";
                }if(ch6.isChecked()==true){
                    if(Marketing.equals(""))
                        Marketing=Marketing+"6";
                    else
                        Marketing=Marketing+",6";
                }if(ch7.isChecked()==true){
                    if(Marketing.equals(""))
                        Marketing=Marketing+"7";
                    else
                        Marketing=Marketing+",7";
                }
                if(ch8.isChecked()==true){
                    if(Marketing.equals(""))
                        Marketing=Marketing+"8";
                    else
                        Marketing=Marketing+",8";
                }
                if(ch9.isChecked()==true){
                    if(Marketing.equals(""))
                        Marketing=Marketing+"9";
                    else
                        Marketing=Marketing+",9";
                }

                args.putString("Fname",fname);
                args.putString("Mname",mname);
                args.putString("Lname",lname);
                args.putString("BirthDate",birthDate);
                args.putString("Country",country);
                args.putString("Prefix",prefix);
                args.putString("PrefferedName",prefferdName);
                args.putString("Email",fname);

                args.putString("Career",carrer);
                args.putString("Course",englishCourse);
                args.putInt("Programm",programm);
                args.putString("GovStudyYear",govStudyYear);
                args.putString("OccupationLast",occupationLast);
                args.putString("DisabilitySupp",disabilitySupp);
                args.putString("Marketing",Marketing);
                args.putString("TermsCon",termsCon);
                args.putString("Gender",gender);

                StudentApplicationFragment4 secFrag = new StudentApplicationFragment4();
                secFrag.setArguments(args);
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });
        return view;
    }
}
