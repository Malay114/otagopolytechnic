package com.example.malay.otagopolytechnic;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.content.Intent;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

import utility.Path;

public class Splash_Activity extends AppCompatActivity {
    SqliteHelper sqliteHelper;
    int type;
    String EmailId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        Path.setPath("http://otagoapp.000webhostapp.com/");
        sqliteHelper =new SqliteHelper(Splash_Activity.this);
        String pwd=sqliteHelper.GetPassword();
        type=sqliteHelper.GetUserType();
        EmailId=sqliteHelper.GetEmail();
        if(!EmailId.equals("")&&type!=2){
            if(type==0){
                LoginFragment.LoginType="Agent";
            }else  if(type==1){
                LoginFragment.LoginType="Student";
            }
        }else{
            LoginFragment.LoginType="";
        }
        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run() {
                try
                {
                    Intent SplashActivityIntent;
                    String id="1";
                    String Title="Home";
                    SplashActivityIntent = new Intent(Splash_Activity.this, Home.class);
                    SplashActivityIntent.putExtra("Page_Id",id);
                    SplashActivityIntent.putExtra("Page_Title",Title);
                    SplashActivityIntent.putExtra("User_Type",type);
                    startActivity(SplashActivityIntent);
                    finish();
                }catch(Exception e){
                }
            }
        },1000);


    }
    @Override
    protected void onDestroy() {

        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(LoginFragment.LoginType.equals("Student")||LoginFragment.LoginType.equals("Agent")){
            menu.getItem(R.id.logIn).setTitle("Logout");
        }
        return super.onCreateOptionsMenu(menu);
    }
}
