package com.example.malay.otagopolytechnic;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class ForgotPasswordFragment extends Fragment {
    View view;
    Button send;
    EditText email;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.forgot_password, container, false);
        setRetainInstance(true);
        email = (EditText) view.findViewById(R.id.edtEmail);

        send = (Button) view.findViewById(R.id.btnSend);

        send.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                String to = email.getText().toString();
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});
                email.putExtra(Intent.EXTRA_SUBJECT, "Otago - Send Password");
                email.putExtra(Intent.EXTRA_TEXT, "Dharti");

                //need this to prompts email client only
                email.setType("message/rfc822");

                //startActivity(Intent.createChooser(email, "Choose an Email client :"));

            }
        });

        return view;
    }

}
