package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class Login  extends Fragment  {
    View view;
    private ProgressDialog progress;
    private OnFragmentInteractionListener mListener;
    private ImageView imge_banner;


    FragmentManager fragmentManager;
    FragmentTransaction ft;
    Fragment frag_Cat_list;
    Fragment frag_title;
    Bundle args;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        DisplayMetrics dm = new DisplayMetrics();
        view = inflater.inflate(R.layout.login, container, false);
        setRetainInstance(true);
        ImageView btnAgent= (ImageView) view.findViewById(R.id.btnaAent);
        ImageView btnStudent= (ImageView) view.findViewById(R.id.btnStudent);
        Button btnSkip=(Button) view.findViewById(R.id.btnSkip);
        args = new Bundle();
        final FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();


        btnSkip.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stuub
                HomeFragment secFrag = new HomeFragment();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();

            }
        });

        btnAgent.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stuub
                LoginFragment secFrag = new LoginFragment();
                args.putString("Login","Agent");
                secFrag.setArguments(args);
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });

        btnStudent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                StudentLoginFragment secFrag = new StudentLoginFragment();
                args.putString("Login","Student");
                secFrag.setArguments(args);
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            //mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }



}
