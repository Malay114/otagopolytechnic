package com.example.malay.otagopolytechnic;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import utility.ListItem;
import utility.Util;


public class StudentApplicationFragment2 extends Fragment implements AdapterView.OnItemSelectedListener {
    private ProgressDialog progress;
    ListView CourseList;

    private List<ListItem> listobj = new ArrayList<>();
    private OnFragmentInteractionListener mListener;
    Spinner spnProgram,spnOccupation;
    View view;
    Button next,previous;
    String fname,lname,mname,birthDate,country,prefix,prefferdName,email,gender;
    EditText carrer;
    int programm;
    Bundle args;
    CheckBox englishCourse,govStudyYear,disabilitySupp,termsCon;
    String EnglishCourse,GovStudyYear,DisabilitySupp,TermsCon,occupationLast;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.student_application_regi2, container, false);
        setRetainInstance(true);

        fname=getArguments().getString("Fname");
        lname=getArguments().getString("Lname");
        mname=getArguments().getString("Mname");
        gender=getArguments().getString("Gender");
        birthDate=getArguments().getString("BirthDate");
        country=getArguments().getString("Country");
        prefix=getArguments().getString("Prefix");
        email=getArguments().getString("Email");
        prefferdName=getArguments().getString("PrefferedName");

        spnProgram=(Spinner)view.findViewById(R.id.spnProgramm);
        List<String>  programmList=new ArrayList<String>();
        programmList.add("Bachelor of Applied Management");
        programmList.add("Bachelor of Design (Honours)");
        programmList.add("Certificate in Foundation Studies (Level 4) (Business)");
        programmList.add("Graduate Diploma in Accounting");
        programmList.add("Graduate Diploma in Applied Management");
        programmList.add("Graduate Diploma in Hotel Management");
        programmList.add("Graduate Diploma in Information Technology");
        programmList.add("Master of Applied Management");
        ArrayAdapter adptr=new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,programmList);
        spnProgram.setAdapter(adptr);
        spnProgram.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                programm= position+1;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        adptr.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        spnOccupation=(Spinner) view.findViewById(R.id.spnOccupation);
        List<String> OccupationList=new ArrayList<>();
        OccupationList.add("College of Education student");
        OccupationList.add("House person or retired");
        OccupationList.add("Non-employed or beneficiary (ex retired)");
        OccupationList.add("Overseas (irrespective of occupation)");
        OccupationList.add("Polytechnic student");
        OccupationList.add("Private Training Establishment student");
        OccupationList.add("Secondary school student");
        OccupationList.add("Self-employed");
        OccupationList.add("University student");
        OccupationList.add("Wage or salary worker");
        OccupationList.add("Wananga student");
        ArrayAdapter adptrOccupation=new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,OccupationList);
        spnOccupation.setAdapter(adptrOccupation);
        spnOccupation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                occupationLast= parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        adptr.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

//        progress = new ProgressDialog(getContext());
//        progress.setCanceledOnTouchOutside(false);
//        progress.setMessage("Loading... ");
//        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        progress.setIndeterminate(true);
//        progress.show();
//
//        String serverName= Path.getPath();
//        String str_url=serverName+"Course_Basic_Info.php?Campus_Id=3";
//
//        new StudentApplicationFragment2.Asynkclass_program().execute(str_url);

        //programm=(CheckBox)view.findViewById(R.id.edtProgramm);

        next=(Button)view.findViewById(R.id.btnNext);

        carrer=(EditText)view.findViewById(R.id.edtIntCareer);

        englishCourse=(CheckBox) view.findViewById(R.id.checkEngCourse);
        govStudyYear=(CheckBox)view.findViewById(R.id.checkGovFirstStdYr);
        disabilitySupp=(CheckBox)view.findViewById(R.id.checkDisSupport);
        termsCon=(CheckBox)view.findViewById(R.id.checkTermCon);
        if(englishCourse.isChecked()==true){
            EnglishCourse="True";
        }else
            EnglishCourse="False";

        if(govStudyYear.isChecked()==true){
            GovStudyYear="True";
        }else
            GovStudyYear="False";
        if(termsCon.isChecked()==true){
            TermsCon="True";
        }else
            TermsCon="False";

        if(disabilitySupp.isChecked()==true){
            DisabilitySupp="True";
        }else
            DisabilitySupp="False";

        args = new Bundle();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StudentApplicationFragment3 secFrag = new StudentApplicationFragment3();
                args.putString("Fname",fname);
                args.putString("Mname",mname);
                args.putString("Lname",lname);
                args.putString("BirthDate",birthDate);
                args.putString("Country",country);
                args.putString("Prefix",prefix);
                args.putString("PrefferedName",prefferdName);
                args.putString("Email",fname);
                args.putString("Gender",gender);

                args.putString("Career",carrer.getText().toString());
                args.putString("OccupationLast",occupationLast);
                args.putString("Course",EnglishCourse);
                args.putInt("Programm",programm);
                args.putString("GovStudyYear",GovStudyYear);
                args.putString("DisabilitySupp",DisabilitySupp);
                args.putString("TermsCon",termsCon.getText().toString());

                secFrag.setArguments(args);
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });

        return view;
    }
    private class Asynkclass_program extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {

            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e("*********************",""+sb.toString());
            return sb.toString();
        }

        protected void onPostExecute(String result)
        {

            JSONObject jsonResponse;
            try
            {
                jsonResponse = new JSONObject(result);
                listobj= Util.jsonTOCourseList(jsonResponse);


            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            progress.hide();

            if(listobj!=null) {

            }
            else {
                Toast.makeText(getContext(),"No data to display", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
