package com.example.malay.otagopolytechnic;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class StudentApplicationFragment1 extends Fragment {
    View view;
    Button next;
    EditText fname,lname,mname,birthDate,prefix,prefferdName,email;
    Bundle args;
    String gender,country;
    Date dob;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.student_application_regi1, container, false);
        setRetainInstance(true);

        next=(Button)view.findViewById(R.id.btnNext);

        Spinner spnCountry=(Spinner)view.findViewById(R.id.spnCountry);
        List<String> countryList=new ArrayList<>();
        countryList.add("Afghanistan");
        countryList.add("Albania");
        countryList.add("Algeria");
        countryList.add("Andorra");
        countryList.add("Angola");
        countryList.add("Antigua and Barbuda");
        countryList.add("Argentina");
        countryList.add("Armenia");
        countryList.add("Australia");
        countryList.add("Austria");
        countryList.add("Bahamas");
        countryList.add("Bahrain");
        countryList.add("Bangladesh");
        countryList.add("Barbados");
        countryList.add("Belarus");
        countryList.add("China");
        countryList.add("Colombia");
        countryList.add("Iceland");
        countryList.add("India");
        countryList.add("Indonesia");
        countryList.add("Iraq");
        countryList.add("Ireland");
        countryList.add("Israel");
        countryList.add("Italy");
        countryList.add("Jamaica");
        countryList.add("Japan");
        countryList.add("Jordan");
        countryList.add("Kazakhstan");
        countryList.add("Kenya");
        countryList.add("Kiribati");
        countryList.add("Korea");

        ArrayAdapter adptrCountry=new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,countryList);
        spnCountry.setAdapter(adptrCountry);
        spnCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                country= parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        adptrCountry.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

//        Gender
        Spinner spnGender=(Spinner)view.findViewById(R.id.spnGender);
        List<String>  genderType=new ArrayList<String>();
        genderType.add("Male");
        genderType.add("Feale");
        ArrayAdapter adptr=new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,genderType);
        spnGender.setAdapter(adptr);
        spnGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                gender= parent.getItemAtPosition(position).toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        adptr.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        fname=(EditText)view.findViewById(R.id.edtFname);
        lname=(EditText)view.findViewById(R.id.edtLname);
        mname=(EditText)view.findViewById(R.id.edtMname);
        birthDate=(EditText)view.findViewById(R.id.edtBdate);
        prefix=(EditText)view.findViewById(R.id.edtPreferedName);
        prefferdName=(EditText)view.findViewById(R.id.edtPreferedName);
        email=(EditText)view.findViewById(R.id.edtEmailId);
        args = new Bundle();
        dob=StudentLoginFragment.DOB;

        fname.setText(StudentLoginFragment.fname);
        lname.setText(StudentLoginFragment.lname);
        email.setText(StudentLoginFragment.EmailId);
        birthDate.setText(dob.toString());


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StudentApplicationFragment2 secFrag = new StudentApplicationFragment2();
                args.putString("Fname",fname.getText().toString());
                args.putString("Mname",mname.getText().toString());
                args.putString("Lname",lname.getText().toString());
                args.putString("BirthDate",birthDate.getText().toString());
                args.putString("Country",country);
                args.putString("Prefix",prefix.getText().toString());
                args.putString("PrefferedName",prefferdName.getText().toString());
                args.putString("Email",email.getText().toString());
                args.putString("Gender",gender);
                secFrag.setArguments(args);
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });
        return view;
    }

}
