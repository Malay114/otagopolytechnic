package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import utility.ListItem;
import utility.Path;
import utility.Util;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link //AbtUs.//OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AbtUs#//newInstance} factory method to
 * create an instance of this fragment.
 */
public class AbtUs extends Fragment implements AdapterView.OnItemClickListener{
    View view;
    ProgressBar pb;
    static int pageId;
    String pageTitle;

    ListView AboutUsList;

    private List<ListItem> listobj = new ArrayList<>();
    private OnFragmentInteractionListener mListener;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        DisplayMetrics dm = new DisplayMetrics();
        view = inflater.inflate(R.layout.fragment_abt_us, container, false);
        setRetainInstance(true);
        AboutUsList = (ListView) view.findViewById(R.id.list1);

        AbtUsListViewAdapter adapter = new AbtUsListViewAdapter(getActivity(), listobj);

        if(listobj.isEmpty()) {
            pb = (ProgressBar) view.findViewById(R.id.pbLoading);
            pb.setVisibility(ProgressBar.VISIBLE);

            String serverName = Path.getPath();
            String str_url = serverName + "List_Contain.php?tbl_name=about_us";

            new Asynkclass_home().execute(str_url);
        }
        else
        {
            AboutUsList.setAdapter(adapter);
            AboutUsList.setOnItemClickListener(AbtUs.this);
        }

        return view;
    }

    private class Asynkclass_home extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {

            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        protected void onPostExecute(String result)
        {

            JSONObject jsonResponse;
            try
            {
                jsonResponse = new JSONObject(result);
                listobj= Util.jsonTOTiteDiscrip(jsonResponse);


            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            pb.setVisibility(ProgressBar.INVISIBLE);

            if(listobj!=null) {
                AbtUsListViewAdapter adapter = new AbtUsListViewAdapter(getActivity(), listobj);
                AboutUsList.setAdapter(adapter);
                AboutUsList.setOnItemClickListener(AbtUs.this);
            }
            else {
                Toast.makeText(getContext(),"No data to display", Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


//        String Block_ID = gridObj.get(position).getId();
//        String Page_ID = gridObj.get(position).getPageId();
//        String Block_Title = gridObj.get(position).getTitle();
//        String Block_Contain = gridObj.get(position).getContain();
//        String Block_Image = gridObj.get(position).getImageData();
//
//        if (null != mListener) {
//            mListener.onItemSelected( Block_ID,Page_ID,Block_Title,Block_Contain,Block_Image);
//        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
