package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import utility.Path;


public class AgentStudentRegiFragment extends Fragment {
    View view;
    Button register;
    ProgressBar pb;
    EditText fname,lname,passportNo,email,password,bdate,agentId;
    SqliteHelper sqliteHelper;
    Bundle args;
    String Aid;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.agent_student_registration, container, false);
        setRetainInstance(true);

        sqliteHelper =new SqliteHelper(getActivity());
        Aid=sqliteHelper.GetPid();



        register=(Button)view.findViewById(R.id.btnRegister);

        fname=(EditText)view.findViewById(R.id.edtStudFname);
        lname=(EditText)view.findViewById(R.id.edtStudLname);
        passportNo=(EditText)view.findViewById(R.id.edtPassport);
        email=(EditText)view.findViewById(R.id.edtStudEmail);
        bdate=(EditText)view.findViewById(R.id.edtBdate);
        args = new Bundle();

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pb = (ProgressBar) view.findViewById(R.id.pbLoading);
                pb.setVisibility(ProgressBar.VISIBLE);
                String serverName= Path.getPath();
                //String str_url=serverName+"New_Student_Agent_Regi.php?First_Name="+fname.getText().toString()+"&Last_Name="+lname.getText().toString()+"&Passport_No="+passportNo.getText().toString()+"&Date_Of_Birth="+bdate.getText().toString()+"&Student_Email_ID="+email.getText().toString()+"&Password="+password.getText().toString()+"&AId="+agentId.getText().toString();
                String str_url=serverName+"New_Student_Agent_Regi.php?FName="+fname.getText().toString()+"&LName="+lname.getText().toString()+"&PNo="+passportNo.getText().toString()+"&DOB="+bdate.getText().toString()+"&Email_Id="+email.getText().toString()+"&AId="+Aid;
                new Asynkclass_Agent_stud_Register().execute(str_url);
            }
        });

        return view;
    }
    private class Asynkclass_Agent_stud_Register extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {
            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        protected void onPostExecute(String result)
        {
            JSONObject jsonResponse;
            String response="";
            try
            {
                jsonResponse = new JSONObject(result);
                response = jsonResponse.getString("info");
//                JSONArray jsonArray = jsonResponse.optJSONArray("info");
//                if(jsonArray!=null) {
//                    List<ListItem> listItem = null;
//                    int length = jsonArray.length();
//                    for (int i = 0; i < length; i++) {
//                        JSONObject jsonChildNode = jsonArray.getJSONObject(i);
//                        Object jsonId = jsonChildNode.getString("Record");
//                        response = jsonId.toString();
//                    }
//                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            pb.setVisibility(ProgressBar.INVISIBLE);

            if(response.equals("New Student Registered.")) {
                Toast.makeText(getContext(),"Student Registered succesfully done", Toast.LENGTH_LONG).show();
                fname.setText("");
                lname.setText("");
                passportNo.setText("");
                email.setText("");
                bdate.setText("");
            }
            else {
                Toast.makeText(getContext(),"Error in Registration", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}

