package com.example.malay.otagopolytechnic;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import utility.ListItem;
import utility.Path;
import utility.Util;

public class HomeFragment extends Fragment{

    View view;
    ProgressBar pb;
    GridView homeList;
    private List<ListItem> listobj = new ArrayList<>();
    private List<ListItem> gridObj = new ArrayList<>();
    private OnFragmentInteractionListener mListener;
    Bundle args;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        DisplayMetrics dm = new DisplayMetrics();
        view = inflater.inflate(R.layout.home_fragment, container, false);
        setRetainInstance(true);
        homeList = (GridView) view.findViewById(R.id.homeGrid);
        GridViewAdapter adapter = new GridViewAdapter(getActivity(), gridObj);

        if(listobj.isEmpty()) {
            pb = (ProgressBar) view.findViewById(R.id.pbLoading);
            pb.setVisibility(ProgressBar.VISIBLE);

            String serverName = Path.getPath();
            String str_url = serverName + "List_Contain.php?tbl_name=page";//"Home_Data.php?";

            new HomeAsynkclass().execute(str_url);
        }
        else
        {
            homeList.setAdapter(adapter);
            homeList.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
                    // TODO Auto-generated method stub
                    if (null != mListener) {
                        String Page_ID = gridObj.get(position).getId();
                        mListener.onItemSelected(Page_ID);
                    }
                }
            });
        }
        return view;
    }
    private class HomeAsynkclass extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {
            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        protected void onPostExecute(String result)
        {
            JSONObject jsonResponse;
            try
            {
                jsonResponse = new JSONObject(result);
                listobj= Util.jsonTOTiteDiscrip(jsonResponse);
                int i=0;
                for(ListItem list:listobj){
                    if(list.getTitle().equals("Home")||(list.getTitle().equals("Apply")&&LoginFragment.LoginType.equals("") )){
                    }
                    else
                    {
                        gridObj.add(list);
                    }
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            //progress.hide();
            pb.setVisibility(ProgressBar.INVISIBLE);

            if(gridObj!=null)
            {
                GridViewAdapter adapter = new GridViewAdapter(getActivity(), gridObj);
                homeList.setAdapter(adapter);

                homeList.setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
                        // TODO Auto-generated method stub
                        if (null != mListener) {

                            String Page_ID = gridObj.get(position).getId();
                            Log.e("_______",Page_ID);
                            mListener.onItemSelected(Page_ID);
                        }
                    }
                });
            }
            else
            {
                Toast.makeText(getActivity(),"No data to display", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
