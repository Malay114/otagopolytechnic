package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import utility.ListItem;
import utility.Path;
import utility.Util;



public class StudentLoginFragment extends Fragment implements AdapterView.OnItemClickListener{
    View view;
    private ProgressDialog progress;
    Button Login;
    String LoginFor;
    EditText StudEmailId,dob;
    TextView forgotPwd;
    private List<ListItem> listobj = new ArrayList<>();
    SqliteHelper sqliteHelper;
    public static String fname,lname,passportNo,EmailId,agentId,sId;
    public static Date DOB;

    private OnFragmentInteractionListener mListener;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        LoginFor=getArguments().getString("Login");

        sqliteHelper =new SqliteHelper(getActivity());
        DisplayMetrics dm = new DisplayMetrics();
        view = inflater.inflate(R.layout.student_firsttime_login_fragment, container, false);
        setRetainInstance(true);

        StudEmailId=(EditText)view.findViewById(R.id.edtStudEmailId);
        dob=(EditText)view.findViewById(R.id.edtDOB);

        Login=(Button)view.findViewById(R.id.btnlogin);
        forgotPwd=(TextView) view.findViewById(R.id.txtForgotPassword);

        forgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotPasswordFragment secFrag = new ForgotPasswordFragment();
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });
        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress = new ProgressDialog(getContext());
                progress.setCanceledOnTouchOutside(false);
                progress.setMessage("Loading... ");
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setIndeterminate(true);
                progress.show();

                String serverName= Path.getPath();
                String str_url=serverName+"Student_Firsttime_Login.php?Email_Id="+StudEmailId.getText().toString()+"&DOB="+dob.getText().toString();
                new Asynkclass_studentLogin().execute(str_url);
            }
        });
        return view;
    }

    private class Asynkclass_studentLogin extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {

            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;

            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            JSONObject jsonResponse;
            String response=new String ();
            try
            {
                jsonResponse = new JSONObject(result);
                response= Util.jsonTOStudLogin(jsonResponse);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            progress.hide();

            if(!response.equals("")) {
                HomeFragment secFrag = new HomeFragment();
                if(LoginFor.equals("Student")){
                    LoginFragment.LoginType="Student";
                    sqliteHelper.InsertLogin(StudEmailId.getText().toString(),"",sId,1);
                    sqliteHelper.InsertStudLogin(fname,lname,passportNo,DOB,EmailId,agentId,sId);
                }else if(LoginFor.equals("Agent")){
                    LoginFragment.LoginType="Agent";
                }
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
            else {
                Toast.makeText(getContext(),"Login Fail, Please veriy login detail", Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
