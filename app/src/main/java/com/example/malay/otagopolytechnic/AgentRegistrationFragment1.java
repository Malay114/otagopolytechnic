package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class AgentRegistrationFragment1 extends Fragment {
    View view;
    Button next;
    EditText fname,lname,mname,agencyName;

    Bundle args;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        DisplayMetrics dm = new DisplayMetrics();
        view = inflater.inflate(R.layout.agent_registration1, container, false);
        setRetainInstance(true);

        next=(Button)view.findViewById(R.id.btnNext);
        fname=(EditText)view.findViewById(R.id.edtFname);
        lname=(EditText)view.findViewById(R.id.edtLname);
        mname=(EditText)view.findViewById(R.id.edtMname);
        agencyName=(EditText)view.findViewById(R.id.edtAgencyName);
        args = new Bundle();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AgentRegistrationFragment2 secFrag = new AgentRegistrationFragment2();
                args.putString("Fname",fname.getText().toString());
                args.putString("Mname",mname.getText().toString());
                args.putString("Lname",lname.getText().toString());
                args.putString("AgencyName",agencyName.getText().toString());
                secFrag.setArguments(args);
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });

        return view;
    }
    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString());
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
    }
    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}

