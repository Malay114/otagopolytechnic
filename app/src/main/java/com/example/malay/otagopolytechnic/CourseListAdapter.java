package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import utility.ListItem;


public class CourseListAdapter  extends ArrayAdapter<ListItem>
{
    private final Activity context;
    private int lastPosition = -1;
    private List<ListItem> courseList;
    public CourseListAdapter(Activity context,List<ListItem> aboutUsList)
    {
        super(context, R.layout.course_single_listview);
        this.context = context;
        this.courseList = aboutUsList;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return courseList.size();
    }
    @Override
    public View getView(int position, View view, ViewGroup parent)
    {

        //Animation animation = AnimationUtils.loadAnimation(getContext(), ((position%2 )==0) ? R.anim.right_from_left : R.anim.left_from_right);

        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.course_single_listview, null);

        ListItem singleItem= courseList.get(position);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt_CourseTitle);
        TextView txtLevelNo = (TextView) rowView.findViewById(R.id.txt_levelNo);
        TextView txtDuration = (TextView) rowView.findViewById(R.id.txt_durationupto);
        TextView txtIntake = (TextView) rowView.findViewById(R.id.txt_intake_month);
        TextView txtApplyw = (TextView) rowView.findViewById(R.id.txt_applywhen);

        txtTitle.setText(singleItem.getCourseTitle());
        txtLevelNo.setText(singleItem.getLevelNo());
        txtDuration.setText(singleItem.getDuration());


        String intake=singleItem.getIntake();
        if(intake==""){rowView.findViewById(R.id.txt_intake).setVisibility(View.GONE);
            rowView.findViewById(R.id.txt_intake_month).setVisibility(View.GONE);}
        else{ txtIntake.setText(singleItem.getIntake());}
        Log.e("*********************",""+singleItem.getIntake());
        txtApplyw.setText(singleItem.getApply());

        /*txtTitle.setOnClickListener(new View.OnClickListener() {
            int flag = 0;

            /*@Override
            public void onClick(View v) {
                if (flag == 0) {
                    txtDescription.setVisibility(View.VISIBLE);
                    flag = 1;
                } else {
                    txtDescription.setVisibility(View.GONE);
                    flag = 0;
                }
            }
        });*/
        //rowView.startAnimation(animation);
        lastPosition = position;
        return rowView;
    }
}