package com.example.malay.otagopolytechnic;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import utility.ListItem;
import utility.Path;


public class AgentRegistrationFragment3 extends Fragment implements AdapterView.OnItemClickListener{
    View view;
    public static String UID="";
    private ProgressDialog progress;
    EditText email, password,confirmPwd;
    Button registration;
    Bundle args;
    String Fname,Mname,Lname,AgencyName,Address1,Address2,Address3,City,Pincode,Country,ContactNo;

    private OnFragmentInteractionListener mListener;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fname=getArguments().getString("Fname");
        Mname=getArguments().getString("Mname");
        Lname=getArguments().getString("Lname");
        AgencyName=getArguments().getString("AgencyName");
        Address1=getArguments().getString("Address1");
        Address2=getArguments().getString("Address2");
        Address3=getArguments().getString("Address3");
        City=getArguments().getString("City");
        Country=getArguments().getString("Country");
        Pincode=getArguments().getString("Pincode");
        ContactNo=getArguments().getString("ContactNo");



        DisplayMetrics dm = new DisplayMetrics();
        view = inflater.inflate(R.layout.agent_registration3, container, false);
        setRetainInstance(true);

        registration=(Button)view.findViewById(R.id.btnAgentRegi);

        email=(EditText)view.findViewById(R.id.edtEmail);

        password=(EditText)view.findViewById(R.id.edtPaswrd);
        confirmPwd=(EditText)view.findViewById(R.id.edtCnfPwd);

        registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress = new ProgressDialog(getContext());
                progress.setCanceledOnTouchOutside(false);
                progress.setMessage("Loading... ");
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setIndeterminate(true);
                progress.show();
                String serverName= Path.getPath();
                String str_url=serverName+"New_Agent_Regi.php?FName="+Fname+"&MName="+Mname+"&LName="+Lname+"&Ag_Name="+AgencyName+"&Ag_Adl1="+Address1+"&Ag_Adl2="+Address2+"&Ag_Adl3="+Address3+"&Ag_City="+City+"&Ag_Pin="+Pincode+"&Ag_Cntry="+Country+"&Ag_PhNo="+ContactNo+"&Email_Id="+email.getText().toString()+"&pass="+password.getText().toString();
                new Asynkclass_Agent_Register().execute(str_url);
            }
        });
        return view;
    }

    private class Asynkclass_Agent_Register extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... urls)
        {
            // TODO Auto-generated method stub
            BufferedReader reader=null;
            StringBuilder sb = new StringBuilder();
            String line = null;
            try
            {
                URL obj=new URL(urls[0]);
                HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
                //conn.setRequestMethod("POST");
                conn.setInstanceFollowRedirects(false);
                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write( urls[0] );
                wr.flush();

                reader= new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while((line = reader.readLine()) != null)
                {
                    sb.append(line + " ");

                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }

        protected void onPostExecute(String result)
        {
            JSONObject jsonResponse;
            String response = new String();
            try
            {
                jsonResponse = new JSONObject(result);
                JSONArray jsonArray = jsonResponse.optJSONArray("info");
                if(jsonArray!=null) {
                    List<ListItem> listItem = null;
                    int length = jsonArray.length();
                    for (int i = 0; i < length; i++) {
                        JSONObject jsonChildNode = jsonArray.getJSONObject(i);
                        Object jsonFname = jsonChildNode.getString("Record");
                        response = jsonFname.toString();
                        AgentRegistrationFragment3.UID=jsonFname.toString();
                    }
                }
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
            progress.hide();
            args = new Bundle();
            if(response.equals("Ok")) {
                Toast.makeText(getContext(),"Registration done successfully.", Toast.LENGTH_LONG).show();
                LoginFragment secFrag = new LoginFragment();
                args.putString("Login","Agent");
                secFrag.setArguments(args);
                FragmentTransaction fragTransaction = getFragmentManager().beginTransaction();
                fragTransaction.replace(R.id.content_main,secFrag );
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
            else {
                Toast.makeText(getContext(),"Registration fail", Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}

